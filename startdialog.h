#ifndef STARTDIALOG_H
#define STARTDIALOG_H

#include <QDialog>
#include "data.h"

namespace Ui {
class StartDialog;
}

class StartDialog : public QDialog
{
    Q_OBJECT

public:
    explicit StartDialog(QWidget *parent = 0);
    ~StartDialog();
    ST_TYPE_SETTING setting_type;
    QString UserTagName;


private slots:
    void on_pushButton_clicked();
    void on_printTypeComboBox_currentIndexChanged(int index);

    void on_startComboBox_currentIndexChanged(int index);

private:
    Ui::StartDialog *ui;
};

#endif // STARTDIALOG_H
