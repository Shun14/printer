#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QKeyEvent>
#include <QDebug>
#include <QLibrary>
#include <QMessageBox>
#include <QCoreApplication>

#include <QTime>
#include <QDateTime>
#include <QFile>
#include <qfiledialog.h>
#include <QTabBar>
#include <QJsonObject>

#include "data.h"

#define LOG_DEBUG  qDebug()<<"["<<__FILE__<<":"<<__LINE__<<__FUNCTION__<<"]"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if( readCfgJson() == false ) {
        QMessageBox::warning(NULL, tr("错误"),tr("配置出错"),QMessageBox::Yes, QMessageBox::Yes);
    }

    /*隐藏tab*/
    //ui->tabWidget->removeTab(1);
    ui->tabWidget->removeTab(2);

    QString fileName_listMix = "./usrConfig/listmix.csv";
    QFile file(fileName_listMix);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"Can't open the file!"<<endl;
        QMessageBox::information(NULL, tr("提示"),tr("listMIx文件无法打开!"),QMessageBox::Yes, QMessageBox::Yes);
    }

    while(!file.atEnd())
    {
        QByteArray line = file.readLine();


        char *str = line.data();
        char str_imei[16] = {0};
        char str_type[50] = {0};
        char str_mode[50] = {0};
        int n = 0;
        QString imei;
        QString type;
        QString mode;

        n = sscanf(str, "%15[0-9],%[^,],%s", str_imei, str_type, str_mode);
        if(n != 3)
        {
            QMessageBox::information(NULL, tr("提示"),tr("records one record error"),QMessageBox::Yes, QMessageBox::Yes);
            continue;
        }

        imei = str_imei;
        type = str_type;
        mode = str_mode;
        qDebug()<< QString("imei:%1 type:%2 mode:%3").arg(imei).arg(type).arg(mode);
        imeiMixList.append(imei);
        typeList.append(type);
        modeList.append(mode);
    }

    //get settings
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");
    //移除批量，注释掉下行就可以看到批量接口

    qDebug()<<"settings:"<<settings.value("productInfo/single").toString();

    if(settings.contains("productInfo/single"))
    {
        ui->productInfo_single->setText(settings.value("productInfo/single").toString());
    }
    if(settings.contains("deviceModel/single"))
    {
        qDebug()<<settings.value("deviceModel/single").toString();
        ui->deviceModelComboBox_single->setCurrentText(settings.value("deviceModel/single").toString());
    }

    if(settings.contains("deviceTypeComboBox/single"))
    {
        ui->deviceTypeComboBox_single->setCurrentText(settings.value("deviceTypeComboBox/single").toString());
    }

    settings.endGroup();
}

MainWindow::~MainWindow()
{
    // 蓝牙钥匙版本
    if(setting_type.label == TYPE_1TAG_TYPE8)
    {
        timer->stop();
        timer_next->stop();
    }

    delete ui;
}

bool MainWindow::readCfgJson() {
    QFile file;
    QString cfgStr = NULL;
    file.setFileName("./usrConfig/cfg.json");
    bool isOpen = file.open(QIODevice::ReadOnly | QIODevice::Text);
    if(isOpen == false) return false;
    cfgStr = file.readAll();
    file.close();

    QJsonDocument document = QJsonDocument::fromJson(cfgStr.toUtf8());
    QJsonObject setting = document.object();
    // add deviceType to single & batch
    if(setting.contains(QString("deviceType")))
    {
        QJsonArray dTval = setting.value(QString("deviceType")).toArray();
        for(int i=0; i< dTval.count(); i++) {
            ui->deviceTypeComboBox_single->addItem(dTval.at(i).toString());
            ui->deviceTypeComboBox_batch->addItem(dTval.at(i).toString());
        }
    }
    else
    {
        //lack of deviceType
        qDebug()<< "deviceType not found in cfg.json";
        return false;
    }

    if(setting.contains(QString("deviceModel"))) {
        QJsonArray dMval = setting.value(QString("deviceModel")).toArray();
        for(int i=0;i< dMval.count();i++) {
            ui->deviceModelComboBox_single->addItem(dMval.at(i).toString());
            ui->deviceModelComboBox_batch->addItem(dMval.at(i).toString());
        }
    } else {
        //lack of deviceModel
        qDebug()<< "deviceModel not found in cfg.json";
        return false;
    }

    return true;
}

int MainWindow::ReconfigUart(void)
{
    switch(setting_type.label)
    {
        case TYPE_1TAG_TYPE8:
        {
            flg_getMAC = false;
            break;
        }

        default:
        {
            break;
        }
    }
    uart_init();

    return 0;
}



int MainWindow::ReconfigUI(void)
{
    switch(setting_type.label)
    {
        case TYPE_MEITUAN:
        {
            ui->label_sn->setText("DeviceId信息:");
            ui->label_imei->setText("MID信息:");
            break;
        }
        case TYPE_1TAG_TYPE8:
        {
            ui->label_imei->setText("MAC信息:");
            break;
        }
        case TYPE_1TAG_TYPE10://433 钥匙
        {
            ui->label_sn->setText("批次信息:");
            ui->label_imei->setText("序列号:");
            break;
        }
        default:
        {
            break;
        }
    }

    return 0;
}

int MainWindow::InitDll()
{
    switch(this->setting_type.printer)
    {
        case TYPE_TSC:
            return init_dll_tsc();
        case TYPE_PTK:
            return init_dll_ptk();
        default:
            return RETURN_LOADDLLERROR;
    }
}

int MainWindow::init_dll_tsc()
{
    QLibrary loadLib("./printerDll/TSCLIB");
    unsigned int flag = 0x0000;
    if(loadLib.load())
    {

        tsc_about = (typ_about)loadLib.resolve("about");
        tsc_openport= (typ_openport)loadLib.resolve("openport");
        tsc_barcode = (typ_barcode)loadLib.resolve("barcode");
        tsc_clearbuffer = (typ_clearbuffer)loadLib.resolve("clearbuffer");
        tsc_closeport = (typ_closeport)loadLib.resolve("closeport");
        tsc_downloadpcx = (typ_downloadpcx)loadLib.resolve("downloadpcx");
        tsc_formfeed = (typ_formfeed)loadLib.resolve("formfeed");
        tsc_nobackfeed = (typ_nobackfeed)loadLib.resolve("nobackfeed");
        tsc_printerfont = (typ_printerfont)loadLib.resolve("printerfont");
        tsc_printlabel = (typ_printlabel)loadLib.resolve("printlabel");
        tsc_sendcommand = (typ_sendcommand)loadLib.resolve("sendcommand");
        tsc_setup = (typ_setup)loadLib.resolve("setup");
        tsc_windowsfont = (typ_windowsfont)loadLib.resolve("windowsfont");
    }
    else
    {
        qDebug()<< "加载动态库失败";
        return RETURN_LOADDLLERROR;
    }
    flag =  tsc_about&&         tsc_openport&&  tsc_barcode&&       tsc_clearbuffer&&   tsc_closeport&& \
            tsc_downloadpcx&&   tsc_formfeed&&  tsc_nobackfeed&&    tsc_printerfont&&   tsc_printlabel&& \
            tsc_sendcommand&&   tsc_setup&&     tsc_windowsfont;
    if(NULL == flag)
    {
        qDebug()<< "缺少动态库函数";
        return RETURN_LOADDLLERROR;
    }

    return RETURN_OK;
}

int MainWindow::init_dll_ptk()
{
    QLibrary loadLib("./printerDll/WINPSK");
    unsigned int flag = 0x0000;

    if(loadLib.load())
    {
        openUsbPort = (fOpenUSBPort)loadLib.resolve("OpenUSBPort");
        closeUsbPort= (fCloseUSBPort)loadLib.resolve("CloseUSBPort");
        PTK_GetErrState = (fPTK_GetErrState)loadLib.resolve("PTK_GetErrState");
        PTK_ClearBuffer = (fPTK_ClearBuffer)loadLib.resolve("PTK_ClearBuffer");
        PTK_SetDirection = (fPTK_SetDirection)loadLib.resolve("PTK_SetDirection");
        PTK_DrawBar2D_QR = (fPTK_DrawBar2D_QR)loadLib.resolve("PTK_DrawBar2D_QR");
        PTK_SetLabelHeight = (fPTK_SetLabelHeight)loadLib.resolve("PTK_SetLabelHeight");
        PTK_SetLabelWidth = (fPTK_SetLabelWidth)loadLib.resolve("PTK_SetLabelWidth");
        PTK_SetDarkness = (fPTK_SetDarkness)loadLib.resolve("PTK_SetDarkness");
        PTK_SetPrintSpeed = (fPTK_SetPrintSpeed)loadLib.resolve("PTK_SetPrintSpeed");
        PTK_DrawRectangle = (fPTK_DrawRectangle)loadLib.resolve("PTK_DrawRectangle");
        PTK_DrawText = (fPTK_DrawText)loadLib.resolve("PTK_DrawText");
        PTK_PrintLabel = (fPTK_PrintLabel)loadLib.resolve("PTK_PrintLabel");
        PTK_PcxGraphicsDownload = (fPTK_PcxGraphicsDownload)loadLib.resolve("PTK_PcxGraphicsDownload");
        PTK_BmpGraphicsDownload = (fPTK_BmpGraphicsDownload)loadLib.resolve("PTK_BmpGraphicsDownload");
        PTK_DrawPcxGraphics = (fPTK_DrawPcxGraphics)loadLib.resolve("PTK_DrawPcxGraphics");
        PTK_DrawTextTrueTypeW = (fPTK_DrawTextTrueTypeW)loadLib.resolve("PTK_DrawTextTrueTypeW");
        PTK_DrawLineOr = (fPTK_DrawLineOr)loadLib.resolve("PTK_DrawLineOr");

        PTK_SetCoordinateOrigin = (fPTK_SetCoordinateOrigin)loadLib.resolve("PTK_SetCoordinateOrigin");
        PTK_DrawBarcode = (fPTK_DrawBarcode)loadLib.resolve("PTK_DrawBarcode");
        PTK_PcxGraphicsDel = (fPTK_PcxGraphicsDel)loadLib.resolve("PTK_PcxGraphicsDel");
        PTK_PrintPCX = (fPTK_PrintPCX)loadLib.resolve("PTK_PrintPCX");
    }
    else
    {
        qDebug()<< "加载动态库失败";
        return RETURN_LOADDLLERROR;
    }

    flag =  openUsbPort && closeUsbPort && PTK_GetErrState && PTK_ClearBuffer && PTK_SetDirection && \
            PTK_DrawBar2D_QR && PTK_SetLabelHeight && PTK_SetLabelWidth && PTK_SetDarkness && PTK_SetPrintSpeed && \
            PTK_DrawRectangle && PTK_DrawText && PTK_PrintLabel && PTK_PcxGraphicsDownload && PTK_BmpGraphicsDownload &&\
            PTK_DrawPcxGraphics && PTK_DrawTextTrueTypeW && PTK_DrawLineOr && PTK_SetCoordinateOrigin && PTK_DrawBarcode && \
            PTK_PcxGraphicsDel && PTK_PrintPCX;
    if(NULL == flag)
    {
        qDebug()<< "缺少动态库函数";
        return RETURN_LOADDLLERROR;
    }
    return RETURN_OK;
}

void MainWindow::on_getImeiButton_clicked()
{
    //只有单张打印和获取IMEI会用到
    QString portComboBox = ui->serialPortComboBox_single->currentText();
    if(portComboBox == "") {
                QMessageBox::information(NULL, tr("错误"),tr("未连接设备或设备未有反应"),QMessageBox::Yes, QMessageBox::Yes);
                addLog(QString("未连接设备或设备未有反应"));
        return;
    }
    //此处是在串口发送函数里面写死了。这里修改QString内容是没用的
    QString comPort = ui->serialPortComboBox_single->currentText().split(" ")[0];

    //TODO:修改发送机制
    //thread.transaction(comPort,1000, QString("7e 0a 00 00"));
}

void MainWindow::addLog(QString str) {
    QString now = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ddd");
    QString log = now + "> " + str;
    static quint64 row = 0;
    int tabIndex = ui->tabWidget->currentIndex() + 1;
    if(tabIndex == 1) {
        ui->listWidget_single->addItem(log);
    } else if (tabIndex == 2) {
        ui->listWidget_batch->addItem(log);
    }
//    LOG_DEBUG<<row;
    ui->listWidget_single->setCurrentRow(row++);
}

void addImeiSnToTxt(QString imei , QString snCode) {
    QFile outFile("./imei2SnCode.txt");
    outFile.open(QIODevice::WriteOnly|QIODevice::Append);
    QTextStream ts(&outFile);
    qDebug()<<"Imei:"<<imei<<"snCode:"<<snCode;
    ts<<imei+"\t"+snCode+"\t"<<endl;
    outFile.close();
}


/**
 * @brief MainWindow::createSingleLabel
 * @param imei 15位
 * @param index imei的次序
 * @return
 * @details 打印指定的imei
 */
int MainWindow::createSingleLabel(QString imei, int index)
{

    imei = imei.split(" ").at(0);
    imei = imei.split("\n").at(0);
    imei = imei.split("\t").at(0);
    qDebug()<<"imei length:"<<imei.length();
    int printTimes = 1;
    int tabIndex = ui->tabWidget->currentIndex() + 1;
    if(tabIndex == 1)
    {
        printTimes = ui->printTimesComboBox_single->currentIndex()+1;
    }
    else if(tabIndex == 2)
    {
        printTimes = ui->printTimesComboBox_batch->currentIndex() + 1;
    }
    qDebug()<<"printTimes:"<< printTimes;
    QString snCode = "";
    bool isSingle = false;
    if(index == -1)
    {
        snCode = ui->productInfo_single->text();
        isSingle = true;
    }
    else
    {
        snCode = ui->productInfo_batch->text();
        if(snCodeList.length() != 0 && snCodeList.at(index) != "" && snCodeList.at(index) != NULL)
        {
            snCode = snCodeList.at(index);
        }
        else
        {
            snCodeSuffix++;
            QString snSuffix = QString("%1").arg(snCodeSuffix, 6, 10, QChar('0'));
            snCode += snSuffix;
        }
    }

    ST_PRINT_INFO printInfo;
    printInfo.imei = imei;
    printInfo.snCode = snCode;
    if(ui->tabWidget->currentIndex() == 0)
    {
        printInfo.lastActiveDate = ui->dateEdit_lastActive_single->date().toString("yyyy-MM-dd");
    }
    else
    {
        printInfo.lastActiveDate = ui->dateEdit_lastActive_batch->date().toString("yyyy-MM-dd");
    }
    qDebug()<< "date:"+printInfo.lastActiveDate;


    if(isSingle == true)
    {
        printInfo.deviceModel = ui->deviceModelComboBox_single->currentText();
        printInfo.selectType = ui->deviceTypeComboBox_single->currentText();
    }
    else
    {
        printInfo.deviceModel = ui->deviceModelComboBox_batch->currentText();
        printInfo.selectType = ui->deviceTypeComboBox_batch->currentText();
    }

    if(imeiMixList.size()!= 0)
    {
        for(int i = 0; i < imeiMixList.size(); i++)
        {
            if(printInfo.imei == imeiMixList.at(i))
            {
                printInfo.deviceModel = modeList.at(i);
                printInfo.selectType = typeList.at(i);
                break;
            }
        }
    }

    int printSuc = 0;

    if(setting_type.printer == TYPE_TSC )
    {
        printSuc = print_lable_tsc(printInfo, printTimes);
    }
    else if(setting_type.printer == TYPE_PTK)
    {
        printSuc = print_lable_ptk(printInfo, printTimes);
    }
    else
    {
        return PRINTER_NOT_OPEN;
    }

    qDebug()<<"printSuc"<<printSuc;
    // return printSuc;
    if(printSuc == PRINTER_NOT_OPEN)
    {
        return PRINTER_NOT_OPEN;
    }
    else if(printSuc != RETURN_OK)
    {
        return PRINTER_NOT_OPEN;
    }
    return RETURN_OK;
}

/* 美团deviceId(sn)转中间码 */
QString MainWindow::meituan_sn_to_middle(QString &deviceId)
{
    QString middle = "";
    if(deviceId.length() > 0)
    {
        middle =middle + (QChar)(((( deviceId.at(deviceId.length()-1).toLatin1() - '0')+5)%10)+'0');
        for(int i = 1; i < deviceId.length()-1; i++)
        {
            middle = middle + (QChar)(((( deviceId.at(i).toLatin1() - '0')+5)%10)+'0');
        }

        middle = middle + (QChar)(((( deviceId.at(0).toLatin1() - '0')+5)%10)+'0');
    }
    return middle;
}

void MainWindow::showResponse(const QString &str)
{
    //过滤imei
    QStringList strList = str.split(QRegExp("[\n]"), QString::SkipEmptyParts);
    //    QString imeiStr = strList.at(strList.length()-1);
    //    QStringList imeiList = imeiStr.split("= ", QString::SkipEmptyParts);
    QString imei = strList.at(strList.length()-1);
    addLog(QString("获取到imei:")+imei);
    if(ui->tabWidget->currentIndex()+1 == 1)
    {
        ui->showImeiText_single->setText(imei);
        if(ui->productInfo_single->text().isEmpty()||ui->productInfo_single->text().length() != 6)
        {
            QMessageBox::critical(NULL, tr("ERROR"), tr("xxxxx"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
        //judge the device model is empty
        if(ui->deviceModelComboBox_single->currentText().isEmpty())
        {
            QMessageBox::critical(NULL, tr("ERROR"), tr(" Please 输入设备型号"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }

        if(ui->showImeiText_single->text().length()!=15)
        {
            QMessageBox::critical(NULL, tr("ERROR"), tr("IMEI长度错误，应该为15,现在:%1").arg(ui->showImeiText_single->text().length()), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }

        //settings
        QSettings settings("xiaoanTech", "Printer");
        settings.beginGroup("MainWindow");
        settings.setValue("productInfo/single",ui->productInfo_single->text());
        settings.setValue("deviceModel/single", ui->deviceModelComboBox_single->currentText());
        settings.setValue("deviceTypeComboBox/single", ui->deviceTypeComboBox_single->currentText());
        settings.endGroup();

        int status = MainWindow::InitDll();
        if(status == RETURN_LOADDLLERROR)
        {
            addLog(QString("add library failed"));
            QMessageBox::critical(NULL, tr("ERROR"), tr ("add library failed"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
        if(ui->showImeiText_single->text() == "")
        {
            return;
        }
        status = MainWindow::createSingleLabel(ui->showImeiText_single->text(), -1);
        if(status == PRINTER_NOT_OPEN)
        {
            addLog(QString("未连接打印机"));
            QMessageBox::critical(NULL, tr("错误"), tr("未连接打印机"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
        else if(status == IMEI_NOT_FOUND)
        {
            addLog(QString("未获取到imei"));
            QMessageBox::critical(NULL, tr("错误"), tr("未获取到imei"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
        else
        {
            addLog(ui->showImeiText_single->text() + QString("打印成功"));
            return;
        }
    }

    if(ui->tabWidget->currentIndex()+1 == 2)
    {
        ui->showImeiText_getImei->setText(imei);
    }
}

void MainWindow::processError(const QString &str)
{
    qDebug()<<"error:"<<str;
}

void MainWindow::processTimeout(const QString &str)
{
    QMessageBox::critical(NULL, tr("错误"), tr("连接设备超时，请拔下串口重新连接或手动输入imei"), QMessageBox::Yes, QMessageBox::Yes);
    addLog(QString("获取imei:")+str);
    qDebug()<<"timeout:"<<str;
}

/**
 * @brief MainWindow::on_exportLabel_single_clicked  butn in tab 1
 * @details 单张打印
 */
void MainWindow::on_exportLabel_single_clicked()  // BUTTON: 生成标签
{
    //    ui->showImeiText_single->setText("");
    QString imei = ui->showImeiText_single->text();
    //串口获取imei
    //on_getImeiButton_clicked();

    // check sn empty  美团 SN -> Device Id 16
    if(TYPE_MEITUAN == this->setting_type.label)
    {
        if((ui->productInfo_single->text().length()!=0)&&(ui->productInfo_single->text().length()!=16))
        {
            QMessageBox::critical(NULL, tr("ERROR"), tr("请检查Device Id，应为十六位数字"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
    }// 蓝牙钥匙
    else if(TYPE_1TAG_TYPE8 == this->setting_type.label)
    {
        /* 不用检查 SN 号 */
    }// 433 钥匙的sn号为批次信息 应为6为前补零数字
    else if(TYPE_1TAG_TYPE10 == this->setting_type.label)
    {
        if( ui->productInfo_single->text().isEmpty() \
         || (ui->productInfo_single->text().length() > 6)\
         || (false == isDigitString(ui->productInfo_single->text())) )
        {
            QMessageBox::critical(NULL, tr("ERROR"), tr("请检查批次信息，应为不多于六位的数字"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
    }
    else if((ui->productInfo_single->text().isEmpty()||ui->productInfo_single->text().length()!=12))
    {
        QMessageBox::critical(NULL, tr("ERROR"), tr("请检查SN，应为十二位数字"), QMessageBox::Yes, QMessageBox::Yes);
        return;
    }

    //judge the device model is empty
    if(ui->deviceModelComboBox_single->currentText().isEmpty())
    {
        QMessageBox::critical(NULL, tr("ERROR"), tr(" Please 输入设备型号"), QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    // check imei length by type
    if(TYPE_MEITUAN == this->setting_type.label)
    {
        QString Did = ui->productInfo_single->text();

        //美团标签16位ID(中间码)  IMEI -> MID 16
        if((imei.length()!=0)&&(imei.length()!=16)) {
            QMessageBox::critical(NULL, tr("ERROR"), tr("MID长度错误，应该为16,现在:%1").arg(imei.length()), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
        /* DID = 0/16  IMEI = 0/16 */
        if(Did.length() != 0)
        {
            QString MID_tmp = meituan_sn_to_middle(Did);
            if(imei.length()!=0)
            {
                if(MID_tmp != imei)// 不匹配
                {
                    addLog("DeviceID 和 MID 不匹配，将使用中间码（MID）打印！");
                }
                 /* 匹配 执行打印 */
            }else{
                // 只有DID
                imei = MID_tmp;
                ui->showImeiText_single->setText(imei);
            }
        }else{
             /* MID, DID 都为空 */
            if(imei.length()==0)
            {
                addLog("请输入 DeviceID 或 MID");
                return;
            }
            /* 使用MID（imei）打印 */
        }
    }
    else if(TYPE_1TAG_TYPE8 == this->setting_type.label)
    {
        /* 蓝牙钥匙mac地址为12位 */
        if(imei.length()!=12) {
            QMessageBox::critical(NULL, tr("ERROR"), tr("MAC 地址长度错误，应该为12,现在:%1").arg(imei.length()), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
    }// 433钥匙 imei
    else if(TYPE_1TAG_TYPE10 == this->setting_type.label)
    {
        if( imei.isEmpty() \
         || (imei.length() > 6)\
         || (false == isDigitString(imei)) )
        {
            QMessageBox::critical(NULL, tr("ERROR"), tr("请检查序列号起始点，应为不多于六位的数字"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
    }
    else
    {
        if(imei.length()!=15) {
            QMessageBox::critical(NULL, tr("ERROR"), tr("IMEI长度错误，应该为15,现在:%1").arg(imei.length()), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
    }

    LOG_DEBUG<<"imei"<<imei;
    //settings
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");
    settings.setValue("productInfo/single",ui->productInfo_single->text());
    settings.setValue("deviceModel/single", ui->deviceModelComboBox_single->currentText());
    settings.setValue("deviceTypeComboBox/single", ui->deviceTypeComboBox_single->currentText());
    settings.endGroup();

    int status = MainWindow::InitDll();
    if(status == RETURN_LOADDLLERROR)
    {
        addLog(QString("add library failed"));
        QMessageBox::critical(NULL, tr("ERROR"), tr ("add library failed"), QMessageBox::Yes, QMessageBox::Yes);
        return;
    }

    bool isRepeat = false;
    qint16 num_tag = ui->spinBox_number->value();       // 打印数量
    do
    {
        if(imei == "")
        {
            LOG_DEBUG<<"err: imei is empty";
            return;
        }
        status = MainWindow::createSingleLabel(imei, -1); // -1 -> single
        if(status == PRINTER_NOT_OPEN)
        {
            addLog(QString("未连接打印机"));
            QMessageBox::critical(NULL, tr("错误"), tr("未连接打印机"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        } else if(status == IMEI_NOT_FOUND)
        {
            addLog(QString("未获取到imei"));
            QMessageBox::critical(NULL, tr("错误"), tr("未获取到imei"), QMessageBox::Yes, QMessageBox::Yes);
            return;
        }
        else
        {
            addLog(QString("\"") + imei + QString("\" 打印成功"));
        }
        // 433钥匙按批次 自增序列号
        if(TYPE_1TAG_TYPE10 == this->setting_type.label)
        {
            if(imei.toInt() < 999999)
            {
                if(--num_tag > 0)
                {
                    imei = QString::number(imei.toInt() + 1);
                    isRepeat = true;
                }
                else
                {
                    isRepeat = false;
                }
            }
            else
            {
                addLog("序列号已满");
                QMessageBox::critical(NULL, tr("错误"), tr("序列号已满"), QMessageBox::Yes, QMessageBox::Yes);
                return ;
            }
        }
    }while(isRepeat);
    return;
}

/**
 * @brief MainWindow::on_exportLabel_batch_clicked
 * @details 批量打印
 */
void MainWindow::on_exportLabel_batch_clicked()
{
    if(ui->productInfo_batch->text().isEmpty()||ui->productInfo_batch->text().length()!=6)
    {
        QMessageBox::critical(NULL, tr("ERROR"), tr("请检查生产信息，应为六位数字"), QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    //judge the device model is empty
    if(ui->deviceModelComboBox_batch->currentText().isEmpty())
    {
        QMessageBox::critical(NULL, tr("ERROR"), tr(" Please 输入设备型号"), QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    //settings
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");
    settings.setValue("productInfo/batch",ui->productInfo_batch->text());
    settings.setValue("deviceModel/batch", ui->deviceModelComboBox_batch->currentText());
    settings.setValue("deviceTypeComboBox/batch", ui->deviceTypeComboBox_batch->currentText());
    settings.setValue("txtPath/batch", ui->txtPath_batch->text());
    settings.endGroup();

    int status = MainWindow::InitDll();
    if(status == RETURN_LOADDLLERROR)
    {
        addLog(QString("add library failed"));
        QMessageBox::critical(NULL, tr("ERROR"), tr ("add library failed"), QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    qDebug()<<"imeiLIst:"<<imeiList.size();
    if(imeiList.size()!=0)
    {
        qint32 i=0;
        qint32 sucSum = 0;
        snCodeSuffix = 0;
        for(;i< imeiList.size(); i++)
        {
            status = MainWindow::createSingleLabel(imeiList.at(i), i);
            if(status == PRINTER_NOT_OPEN)
            {
                addLog(QString("未连接打印机"));
                QMessageBox::critical(NULL, tr("错误"), tr("未连接打印机,请连接后重新打印"), QMessageBox::Yes, QMessageBox::Yes);
                break;
            }
            else if(status == IMEI_NOT_FOUND)
            {
                addLog(QString("未获取到imei"));
                QMessageBox::critical(NULL, tr("错误"), tr("未获取到imei"), QMessageBox::Yes, QMessageBox::Yes);
            }
            else
            {
                sucSum ++;
                addLog(imeiList.at(i) + QString("打印成功"));
            }
        }

        imeiList.clear();//清空imei
        snCodeList.clear();
        ui->txtPath_batch->text().clear();
        QMessageBox::information(NULL, tr("提示"),tr("共打印imei:%1").arg(sucSum),QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    else
    {
        addLog(QString("打印失败"));
        QMessageBox::information(NULL, tr("提示"),tr("共打印imei:%1").arg(imeiList.size()),QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
}

void MainWindow::on_getImeiButton_getImei_clicked()
{
    on_getImeiButton_clicked();

}

/**
 * @brief MainWindow::on_getTxtPushButton_batch_clicked
 * @details 获取txt文件路径
 */
void MainWindow::on_getTxtPushButton_batch_clicked()
{
    imeiList.clear();
    snCodeList.clear();
    ui->listWidget_batch->clear();
    txtFileName = QFileDialog::getOpenFileName(this,
                                               tr("选择txt文件"),
                                               "",
                                               tr("Txt (*.txt)")); //选择路径
    ui->txtPath_batch->setText(txtFileName);
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");
    settings.setValue("txtPath/batch", ui->txtPath_batch->text());
    settings.endGroup();
    QFile file(txtFileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"Can't open the file!"<<endl;
        QMessageBox::information(NULL, tr("提示"),tr("文件无法打开!"),QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        char *str = line.data();
        char str_imei[16] = {0};
        char str_snCode[17] = {0};
        int n = 0;
        QString imei;
        QString snCode;

        if(TYPE_MEITUAN == this->setting_type.label)
        {   /* meituan: IMEI->15, SN/DID->16 */
            n = sscanf(str, "%15[0-9]%*[^0-9]%16[0-9]", str_imei, str_snCode);
        }
        else
        {
            /* IMEI->15, SN->12 */
            n = sscanf(str, "%15[0-9]%*[^0-9]%12[0-9A-Za-z]", str_imei, str_snCode);
        }

        if(n != 2)
        {
            QMessageBox::information(NULL, tr("提示"),tr("records one record error"),QMessageBox::Yes, QMessageBox::Yes);
            continue;
        }

        imei = str_imei;
        snCode = str_snCode;
        addLog(QString("imei:%1 snCode:%2").arg(imei).arg(snCode));

        imeiList.append(imei);
        snCodeList.append(snCode);
    }
    file.close();
    QMessageBox::information(NULL, tr("提示"),tr("共有imei:%1,请输入六位生产信息数字作为sncode前缀").arg(imeiList.size()),QMessageBox::Yes, QMessageBox::Yes);
}

#include <stdio.h>
// 单张打印 选取txt文件
void MainWindow::on_single_pushButton_choseFile_clicked()
{
    single_imeiList.clear();
    single_snCodeList.clear();
    addLog("------------------打开文件---------------------");
//    ui->listWidget_single->clear();
    single_txtFileName = QFileDialog::getOpenFileName(this,
                                               tr("选择txt文件"),
                                               "",
                                               tr("Txt (*.txt)")); //选择路径
    ui->single_lineEdit_file->setText(single_txtFileName);
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");
    settings.setValue("txtPath/single", ui->single_lineEdit_file->text());
    settings.endGroup();
    QFile file(single_txtFileName);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug()<<"Can't open the file!"<<endl;
        QMessageBox::information(NULL, tr("提示"),tr("文件无法打开!"),QMessageBox::Yes, QMessageBox::Yes);
        return;
    }
    quint64 Line = 0;
    while(!file.atEnd())
    {
        QByteArray line = file.readLine();
        char *str = line.data();
        char str_imei[16] = {0};
        char str_snCode[17] = {0};
        int n = 0;
        QString imei;
        QString snCode;
        Line++;
        //QString str(line);

        /*QStringList list = str.split(' ', QString::SkipEmptyParts);
        QString imei = list.at(0);
        QString snCode = NULL;
        if(list.length() > 1)
        {
            QStringList hasSn = list.at(1).split('\n', QString::SkipEmptyParts);
            if(list.length() > 1&&hasSn.length() > 0) snCode = hasSn.at(0);
        }*/

        if(TYPE_MEITUAN == this->setting_type.label)
        {   /* meituan: IMEI->15, SN/DID->16 */
            n = sscanf(str, "%15[0-9]%*[^0-9]%16[0-9]", str_imei, str_snCode);
        }
        else{
            n = sscanf(str, "%15[0-9]%*[^0-9A-Za-z]%12[0-9A-Za-z]", str_imei, str_snCode);
        }

        if(n != 2)
        {
            LOG_DEBUG << "HERE";
            addLog(QString("无效行:   ") + QString::number(Line));
            QMessageBox::information(NULL, tr("提示"),\
                tr("records one record error: Line->")+QString::number(Line),QMessageBox::Yes, QMessageBox::Yes);
            continue;
        }
//       if(Line %100 == 0)
//       {
//           LOG_DEBUG << "HERE " << Line;
//       }
        imei = str_imei;
        snCode = str_snCode;
        if(TYPE_MEITUAN == this->setting_type.label)
        {
            LOG_DEBUG<< imei.length() << snCode.length();
            if((imei.length()!=15)|| (snCode.length()!=16))
            {
                addLog(QString("请检查数据行：  ") + QString::number(Line));
                continue ;
            }
            if(Line <= 80)
            {
                addLog(QString::number(Line) + QString("> IMEI:%1    DeviceID:%2").arg(imei).arg(snCode));
            }
        }
        else{
            if(Line <= 80)
            {
                addLog(QString::number(Line) + QString("> imei:%1    snCode:%2").arg(imei).arg(snCode));
            }
        }


        single_imeiList.append(imei);
        single_snCodeList.append(snCode);
    }
    file.close();
    addLog(QString("从文件录入成功数/总行数： ")+ QString::number(single_imeiList.size())\
           + QString("/")+ QString::number(Line));
    QMessageBox::information(NULL, tr("提示"),tr("load %1 records success").arg(single_imeiList.size()),QMessageBox::Yes, QMessageBox::Yes);
}


// 扫码枪串口调
void MainWindow::readData()
{
    static QByteArray buf;
    buf += serial->readAll(); // serial->readAll().toHex() 转换成16进制形式
    qDebug()<<buf;

    QString IMEISTR = buf;
    QString IMEI;
    QString SN = "";

    LOG_DEBUG<<IMEISTR;

    // 扫码的识别处理
    // 自家二维码通用扫码
    int i = IMEISTR.indexOf("IMEI:",0);
    if( (i >= 0) && (buf.length() >= i+5+15+4) ) //"IMEI:xxx(15) SN:xxxx(12)"
    {
        int j = IMEISTR.indexOf("SN:",0);
        if( (j >= i)&&(buf.length() >= i+5+15+4+12))
        {
            SN = IMEISTR.mid(j+3,12);
        }
        else
        {
            if(buf.length() > i+5+15+4+12) // 可能是扫芯片
            {
                qDebug()<<"not found sn";
            }
            else
            {
                return; //continue
            }
        }

        // get imei
        IMEI = IMEISTR.mid(i+5,15);
        if(!SN.isEmpty())
        {
            addLog("读到 SN:" + SN);
            if(setting_type.label == TYPE_1TAG_TYPE2)
            {
                setting_type.label = TYPE_1TAG_TYPE3;
            }
        }
        addLog("读到 IMEI:" + IMEI);
        buf.clear();
    }
    else if((15 == IMEISTR.indexOf(";",0)) && 2 <= IMEISTR.count(';')) // 4G ECXX 芯片扫码   "imei(15);xxx;xxx"
    {
        IMEI = IMEISTR.mid(0,15);
        addLog("读到 IMEI:" + IMEI);
        buf.clear();
    }
    else if(0 == IMEISTR.indexOf("86",0))// 86开头的 可能是IMEI号码
    {
        int i = 0;
        for(i = 0; i < 15; i++){
            if(IMEISTR[i] >= '0' && IMEISTR[i] <= '9'){
                continue;
            } else {
                break;
            }
        }

        if(i >= 15){
            //是IMEI号码
            IMEI = IMEISTR.mid(0,15);
            addLog("读到 IMEI:" + IMEI);
            buf.clear();
        } else {
            //搞错了 再来
            QMessageBox::information(NULL, tr("error"),tr("decode imei error, please try again"),QMessageBox::Yes, QMessageBox::Yes);
            buf.clear();
            return;
        }
    }
    else  // 美团格式扫码
    {
        LOG_DEBUG<<"buf.length()"<<buf.length();
        /* 美团 扫中间码 */
        if((buf.length() == 17) && (TYPE_MEITUAN == this->setting_type.label)) // MID + '\r'
        {
            IMEI =IMEISTR.mid(0,16);
            LOG_DEBUG<<"meituan: copy one"<<IMEI;
            addLog("读到 MID:" + IMEI);
            buf.clear();
        }
        else if(buf.length() > 256)
        {
            addLog("decode imei error");
            QMessageBox::information(NULL, tr("error"),tr("decode imei error, please try again"),QMessageBox::Yes, QMessageBox::Yes);
            buf.clear();
            return;
        }
        else
        {
            buf.clear();
            return;
        }
    }

    if(TYPE_1TAG_TYPE11 == this->setting_type.label)
    {
        LOG_DEBUG<<IMEI.length();
        ui->showImeiText_single->setText(IMEI);
        addLog("测试完成");
        return;
    }



    if(TYPE_MEITUAN == this->setting_type.label)
    {
        if(SN.length())
        {
            addLog("不是美团二维码");
            return;
        }
    }
    LOG_DEBUG<<IMEI.length();
    if((TYPE_MEITUAN != this->setting_type.label)|| (IMEI.length() == 16))
    {
        ui->showImeiText_single->setText(IMEI);
        if(TYPE_MEITUAN == this->setting_type.label)
        {
            ui->productInfo_single->setText("");
        }
    }

    if(SN.length() == 12)
    {
        ui->productInfo_single->setText(SN);
    }else if((TYPE_MEITUAN == this->setting_type.label) && (IMEI.length() == 16)) // get MeiTuan MID
    {
        LOG_DEBUG<<"get MeiTuan MID";
    }
    else
    {
        qDebug("search sn from list");
        if(single_imeiList.size() > 0)
        {
            int j = 0;
            j = single_imeiList.indexOf(IMEI);
            if(j >= 0) /* found imei from list, add its sn(Did) to text */
            {
                ui->productInfo_single->setText(single_snCodeList.at(j));
                if(TYPE_MEITUAN == this->setting_type.label)
                {
                    addLog("找到 DeviceID:" + single_snCodeList.at(j));
                    ui->showImeiText_single->setText("");
                }else
                {
                    addLog("找到 sn:" + single_snCodeList.at(j));
                    qDebug()<<single_snCodeList.at(j);
                }
            }
            else
            {
                addLog(tr("SN/DeviceID not exist，please check IMEI and file"));
                QMessageBox::information(NULL, tr("error"),tr("sn not exist，please check IMEI and file"),QMessageBox::Yes, QMessageBox::Yes);
                return;
            }

        }
        else
        {
            QMessageBox::information(NULL, tr("error"),tr("record not exit, please check file"),QMessageBox::Yes, QMessageBox::Yes);
            addLog(tr("Record not exit, please check file"));
            return;
        }
    }

    this->on_exportLabel_single_clicked();
}



void MainWindow::sendData()
{
    //TODO:
}

void MainWindow::on_serialPortComboBox_single_activated(const QString &arg1)
{
    if(!arg1.isEmpty())
    {
        QString comPort = ui->serialPortComboBox_single->currentText().split(" ")[0];
        if(serial == NULL)
        {
            serial = new QSerialPort;
        }

        //if(serial->portName() != comPort)
        {
            serial->close();
            serial->setPortName(comPort);  // 指定端口
            serial->setBaudRate(QSerialPort::Baud115200); // 设置波特率
            serial->setDataBits(QSerialPort::Data8); // 设置位数
            serial->setParity(QSerialPort::EvenParity); // 设置偶校验
            serial->setStopBits(QSerialPort::OneStop); // 设置一个停止位
            serial->setFlowControl(QSerialPort::NoFlowControl); // 设置无流控制
            serial->open(QIODevice::ReadWrite); // 读写模式打开串口
            connect(serial,SIGNAL(readyRead()),this,SLOT(readData())); // 连接串口的信号和槽函数

            addLog(("change connect to" + comPort));
        }
    }
}



