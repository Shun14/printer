#include "startdialog.h"
#include "ui_startdialog.h"
#include <QComboBox>
#include <QIcon>
#include <QDebug>

#define LOG_DEBUG  qDebug()<<"["<<__FILE__<<":"<<__LINE__<<__FUNCTION__<<"]"

StartDialog::StartDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::StartDialog)
{
    ui->setupUi(this);
    this->setting_type.label = TYPE_LABEL_NONE;
    this->setting_type.printer = TYPE_TSC;
    ui->pushButton->setEnabled(false);
}


StartDialog::~StartDialog()
{
    delete ui;
}

void StartDialog::on_pushButton_clicked()
{
    if("POSTEK" == ui->startComboBox->currentText())
    {
        this->setting_type.printer = TYPE_PTK;
        accept();
    }

    if("TTP244PRO" == ui->startComboBox->currentText())
    {
        this->setting_type.printer = TYPE_TSC;
        accept();
    }
}


void StartDialog::on_printTypeComboBox_currentIndexChanged(int index)
{
    QImage img;
    if(ui->startComboBox->currentText() == "POSTEK")
    {
        ui->printTypeComboBox->setCurrentIndex(3);
        index = 3;
        this->setting_type.label = TYPE_3TAG_TYPE1;
    }

    switch(index)
    {
        case 1:
            //   单排亚银纸  小安经典款 3张
            this->setting_type.label = TYPE_1TAG_TYPE4;
            img.load("./icon/type2.png");
            this->UserTagName = "单列 30X15X1 样式1";
            break;

        case 2:
            //type10: 为哑银单列 蓝牙钥匙打码
            this->setting_type.label = TYPE_1TAG_TYPE8;
            img.load("./img/单列 30X15X1 样式2.jpg");
            this->UserTagName = "单列 30X15X1 样式2";
        break;

        case 3:
            //type13: 为哑银单列 G806 图标换型号
            this->setting_type.label = TYPE_1TAG_TYPE9;
            img.load("./img/单列 30X15X1 样式3.jpg");
            this->UserTagName = "单列 30X15X1 样式3";
        break;

        case 4:
            //type14: 为哑银单列 K901 二维码换成钥匙logo
            this->setting_type.label = TYPE_1TAG_TYPE10;
            img.load("./img/单列 30X15X1 样式4.jpg");
            this->UserTagName = "单列 30X15X1 样式4";
        break;

        case 5:
            //type13: 经典样式 单列
            this->setting_type.label = TYPE_1TAG_TYPE11;
            img.load("./img/单列 35X20X1 样式1.jpg");
            this->UserTagName = "单列 35X20X1 样式1";
        break;

        case 6:
            //   单排 40*30 大纸  小蜜
            this->setting_type.label = TYPE_1TAG_TYPE6;
            img.load("./img/单列 40X30X1 样式1.jpg");
            this->UserTagName = "单列 40X30X1 样式1";
            break;

        case 7:
            //type14: 经典样式  双列
            this->setting_type.label = TYPE_2TAG_TYPE2;
            img.load("./img/双列 35X20X2 样式1.jpg");
            this->UserTagName = "双列 35X20X2 样式1";
        break;

        case 8: // 经典样式 单列 老接口
            this->setting_type.label = TYPE_1TAG_TYPE1;
            img.load("./img/单列 35X20X1 样式1.jpg");
            this->UserTagName = "单列 35X20X1 样式1";
            break;

        case 13:
            // type : 4G ECXX 扫码测试预览
            this->setting_type.label = TYPE_1TAG_TYPE11;
        break;

        default:
            ui->labelPicView->clear();
            ui->pushButton->setEnabled(false);
            this->setting_type.label = TYPE_LABEL_NONE;
            return;
            break;
    }

    qDebug()<<index;
    QPixmap mp;
    mp = mp.fromImage(img);
    ui->labelPicView->clear();
    ui->labelPicView->setPixmap(mp);
    ui->labelPicView->resize( QSize(img.width(),img.height()) );
    ui->labelPicView->show();
    ui->pushButton->setEnabled(true);
}

void StartDialog::on_startComboBox_currentIndexChanged(int index)
{
    if(ui->startComboBox->currentText() == "POSTEK")
    {
        ui->printTypeComboBox->setCurrentIndex(3);
        this->setting_type.label = TYPE_3TAG_TYPE1;
        on_printTypeComboBox_currentIndexChanged(3);
    }
}
