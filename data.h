#ifndef DATA_H
#define DATA_H

#include <QString>



#define TAG_TYPE 1

#define TSC_1TAG_TYPE 1
#define TSC_3TAG_TYPE 1

#define PTK_1TAG_TYPE 1
#define PTK_3TAG_TYPE 1

#define MEITUAN_IS_SN (1)

typedef enum
{
    TYPE_TSC,
    TYPE_PTK,
    TYPE_PRINTER_NONE
}ENUM_PRINTER_TYPE;

typedef enum
{
    TYPE_1TAG_SILVER = 0,
    TYPE_1TAG_TYPE2 = TYPE_1TAG_SILVER,
    TYPE_1TAG_TYPE3,
    TYPE_1TAG_TYPE4,
    TYPE_1TAG_TYPE7,
    TYPE_1TAG_TYPE8,
    TYPE_1TAG_TYPE9,
    TYPE_1TAG_TYPE10,
    TYPE_1TAG_TYPE11,

    TYPE_1TAG ,
    TYPE_1TAG_TYPE1 = TYPE_1TAG,
    TYPE_1TAG_TYPE5,
    TYPE_MEITUAN,

    TYPE_1TAG_SILVER_40_30,
    TYPE_1TAG_TYPE6 = TYPE_1TAG_SILVER_40_30,

    TYPE_3TAG,
    TYPE_3TAG_TYPE1 = TYPE_3TAG,

    TYPE_2TAG,
    TYPE_2TAG_TYPE1 = TYPE_2TAG,
    TYPE_2TAG_TYPE2,

    TYPE_LABEL_NONE

}ENUM_LABEL_TYPE;

typedef enum
{
    P1_T1,
    P1_T2,
    P1_T3,
    P30X15X1,

    P2_T1,
    P2_T2,
    P35X20X1,

    P3_T1,
    P3_T2,
    P40X30X1,

    P4_T1,
    P35X20X2,

    P_T_NONE,
}ENUM_PAPER_TYPE;


typedef struct
{
    QString imei;
    QString snCode;
    QString lastActiveDate;
    QString deviceModel;   // 设备型号  "AT-XXX", "K920"
    QString selectType;    // 设备类型 "数据终端", "ECU"
}ST_PRINT_INFO;

typedef struct
{
    ENUM_PRINTER_TYPE printer;
    ENUM_LABEL_TYPE label;
}ST_TYPE_SETTING;



#endif // DATA_H
