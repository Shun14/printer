#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QKeyEvent>
#include <QDebug>
#include <QLibrary>
#include <QMessageBox>
#include <QCoreApplication>

#include <QTime>
#include <QDateTime>
#include <QFile>
#include <qfiledialog.h>
#include <QTabBar>
#include <QJsonObject>

#include "data.h"

#define LOG_DEBUG  qDebug()<<"["<<__FILE__<<":"<<__LINE__<<__FUNCTION__<<"]"

// 判断是否为纯中文
bool IsChinese(QString &qstrSrc)
{
    int nCount = qstrSrc.count();
    bool bret = true;
    for(int i=0; i<nCount; ++i)
    {
        QChar cha = qstrSrc.at(i);
        ushort uni = cha.unicode();
        if(uni >= 0x4E00 && uni <= 0x9FA5)
        {

        }
        else
        {
            bret = false;
            break;
        }
    }
    return bret;
}

bool MainWindow:: isDigitString(const QString& src)
{
    const char *s = src.toUtf8().data();
    while(*s && *s>='0' && *s<='9')s++;
    return !bool(*s);
}

// 280 * 160   25 * 60
//type1:打印设备类型、型号、imei、sn、QRcode用于小蜜芒果等热敏设备标签
int MainWindow::print_1tag_lable_tsc_type1(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    quint8 size_model = (printInfo.deviceModel.length()<8)? 30 : 24;
    quint8 start_model ;  // 注意除法取整
    quint8 offsety_model;
    if(IsChinese(printInfo.deviceModel)) // 中文大小为方形
    {
        size_model = (printInfo.deviceModel.length() < 5)? 28 : 24;
        start_model = (280 + 120)/2.0  - printInfo.deviceModel.length() * size_model/2.0;
        offsety_model = (printInfo.deviceModel.length() < 5)? 0 : 4;
    }
    else  // ascii 的宽度为高度一半
    {
        start_model = (280 + 105)/2.0  - (printInfo.deviceModel.length() / 2.0) * size_model/2.0;
        offsety_model = (printInfo.deviceModel.length() < 8)? 0 : 7;
    }
    errCode = tsc_windowsfont(start_model, 5 + offsety_model, size_model, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
//    errCode = tsc_windowsfont(138, 5, 30, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    // 设备类型
    QString name =  printInfo.selectType;
    quint8 size = 33;
    quint8 start ; // 注意除法取整
    if(IsChinese(name)) // 中文大小为方形
    {
        start = (280 + 120)/2.0  - name.length() * size/2.0;
    }
    else  // ascii 的宽度为高度一半
    {
        start = (280 + 120)/2.0  - (name.length() / 2.0) * size/2.0;
    }
    errCode = tsc_windowsfont( start, 80 - size/2.0, size,0,0,0, "Arial", name.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_sendcommand("DIAGONAL 132, 33, 256, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    QString data = QString("QRCODE 12,5,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}

//type7:双排35*20mm亚银标签纸
int MainWindow::print_2tag_lable_tsc_type1(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;
    QString qrCodeData;
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

//    errCode = tsc_sendcommand("OFFSET 0 dot");
//    qDebug()<<"cmd_OFFSET:"<<errCode;

    //打印内容1 reference = 2mm = 16dot
    errCode = tsc_sendcommand("REFERENCE 24,0");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(138, 5, 30, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    errCode = tsc_windowsfont(150,65,33,0,0,0,"Arial", printInfo.selectType.toLocal8Bit().data());//73-8
    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_sendcommand("DIAGONAL 132, 33, 256, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    qrCodeData = QString("QRCODE 12,5,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(qrCodeData.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    //打印内容2 reference: 2mm+35mm+2mm = 39mm = 312
    errCode = tsc_sendcommand("REFERENCE 320,0");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(138, 5, 30, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    errCode = tsc_windowsfont(150,65,33,0,0,0,"Arial", printInfo.selectType.toLocal8Bit().data());//73-8
    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_sendcommand("DIAGONAL 132, 33, 256, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    //QString data = QString("QRCODE 12,5,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(qrCodeData.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}

//type2:打印无设备类型、型号，只有QRcode，可以打印barcode
int MainWindow::print_1tag_lable_tsc_type2(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;
//    int deviceModelx, deviceModely;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("REFERENCE 0,8");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(120, 5, 16, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_windowsfont(120, 90, 20,0,2,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

//    //deviceModelx = 120; //120-360 AT_MG1E  //1~10BYTE big:24
//    deviceModely = 40;
//    if(printInfo.deviceModel.length() > 10)
//    {
//       deviceModelx = 120;
//    }
//    else
//    {
//        qDebug()<<"length:"<< printInfo.deviceModel.length();

//        deviceModelx = 173-(printInfo.deviceModel.length() * 13)/2;
//    }
//    errCode = tsc_windowsfont(deviceModelx, deviceModely, 24,0,2,0,"Arial", printInfo.deviceModel.toLocal8Bit().data());
//    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    QString data = QString("QRCODE 12,8,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");

    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    //PUTBMP x,y,"filename"[, bpp][, contract]
    errCode = tsc_sendcommand("PUTBMP 136,32,\"logo.BMP\"");
    qDebug()<<"PUTBMP:"<<errCode;

    return RETURN_OK;
}

int MainWindow::print_1tag_lable_tsc_type5(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    errCode = tsc_windowsfont(138, 5, 30, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    if( printInfo.selectType == tr("小蜜") )
    {
        errCode = tsc_sendcommand("PUTBMP 150,65,\"MEGRAY.BMP\"");
        qDebug()<<"PUTBMP:"<<errCode;

        qDebug()<< "is xiaomi, chose MEGRAY.BMP";
    }
    else if( printInfo.selectType == tr("芒果") )
    {
        errCode = tsc_sendcommand("PUTBMP 130,65,\"MGGRAY.BMP\"");
        qDebug()<<"PUTBMP:"<<errCode;

        qDebug()<< "is xiaomi, chose MEGRAY.BMP";
    }
    else
    {
        errCode = tsc_windowsfont(150,65,33,0,0,0,"Arial", printInfo.selectType.toLocal8Bit().data());//73-8
        qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;
    }

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_sendcommand("DIAGONAL 132, 33, 256, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    QString data = QString("QRCODE 12,5,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}

int MainWindow::print_1tag_lable_tsc_type6(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    errCode = tsc_windowsfont(13, 13, 22, 0, 0, 0, "Arial", QString("设备号："+ printInfo.imei).toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_windowsfont(189, 140, 22, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("DIAGONAL 189, 165, 189 + 111, 165 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    errCode = tsc_windowsfont(189, 177, 20,0,2,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    errCode = tsc_windowsfont(39, 212, 20,0,0,0,"Arial", QString( "  GPS").toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont:"<<errCode;

    errCode = tsc_windowsfont(206, 212, 20,0,0,0,"Arial", QString("  GPRS").toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont:"<<errCode;


    if( printInfo.selectType == tr("小蜜") )
    {
        errCode = tsc_sendcommand("PUTBMP 214,62,\"MEGRAY.BMP\"");
        qDebug()<<"PUTBMP:"<<errCode;

        qDebug()<< "is xiaomi, chose MEGRAY.BMP";
    }
    else if( printInfo.selectType == tr("芒果") )
    {
        errCode = tsc_sendcommand("PUTBMP 130,65,\"MGGRAY.BMP\"");
        qDebug()<<"PUTBMP:"<<errCode;

        qDebug()<< "is xiaomi, chose MEGRAY.BMP";
    }
    else
    {
        errCode = tsc_windowsfont(150,65,33,0,0,0,"Arial", printInfo.selectType.toLocal8Bit().data());//73-8
        qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;
    }

    QString data = QString("QRCODE 13,44,M,6,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}

int MainWindow::print_1tag_lable_tsc_type3(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;
    QString dateInfo = "最后激活日期：" + printInfo.lastActiveDate;
    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("REFERENCE 0,8");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(20, 8, 16,0,0,0,"Arial", dateInfo.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

//    LOG_DEBUG<<"SN:"<<printInfo.snCode<<"TO8BIT"<<printInfo.snCode.toLocal8Bit()<<"DATA:"<< printInfo.snCode.toLocal8Bit().data();
    errCode = tsc_barcode("20","24","EAN13","60","2","0","2","2",printInfo.snCode.toLocal8Bit().data());

    return RETURN_OK;
}

int MainWindow::print_1tag_lable_tsc_type7(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;
//    int deviceModelx, deviceModely;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("REFERENCE 0,8");
    qDebug()<<"cmd_REFERENCE:"<<errCode;


    // 设备类型 AT-MT1
    QString name = "  " + printInfo.deviceModel;
    errCode = tsc_windowsfont(105, 8, 30, 0, 2, 0, "Arial", name.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("BAR 95, 35, 133, 4");//140-8
    qDebug()<<"draw line:"<<errCode;

    errCode = tsc_windowsfont(95, 40, 22,0,2,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    errCode = tsc_windowsfont(19, 85, 28, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_windowsfont(120, 55, 30, 0, 2, 0, "Arial", printInfo.selectType.toLocal8Bit().data());
    qDebug()<<"printerfont,selectType:"<<errCode;


//    //deviceModelx = 120; //120-360 AT_MG1E  //1~10BYTE big:24
//    deviceModely = 40;
//    if(printInfo.deviceModel.length() > 10)
//    {
//       deviceModelx = 120;
//    }
//    else
//    {
//        qDebug()<<"length:"<< printInfo.deviceModel.length();

//        deviceModelx = 173-(printInfo.deviceModel.length() * 13)/2;
//    }
//    errCode = tsc_windowsfont(deviceModelx, deviceModely, 24,0,2,0,"Arial", printInfo.deviceModel.toLocal8Bit().data());
//    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    QString data = QString("QRCODE 12,8,M,3,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");

    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}

int MainWindow::print_3tag_lable_tsc_type1(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    //打印内容1
    errCode = tsc_sendcommand("REFERENCE 8,0");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(120, 5, 16, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_windowsfont(120, 90, 20,0,2,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    QString data = QString("QRCODE 12,8,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    //打印内容2
    errCode = tsc_sendcommand("REFERENCE 264,0");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(120, 5, 16, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    errCode = tsc_windowsfont(120, 90, 20,0,2,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    data = QString("QRCODE 12,8,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    //打印内容3
    errCode = tsc_sendcommand("REFERENCE 520,0");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_barcode("20","16","128","60","2","0","2","2", printInfo.snCode.toLocal8Bit().data());

    return RETURN_OK;
}

int MainWindow::print_1tag_lable_ptk_type1(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    errCode = PTK_ClearBuffer();
    qDebug()<<"PTK_ClearBuffer:"<<errCode;

    //draw imei
    errCode = PTK_DrawText(178, 7, 0, 3, 1, 1, 'N', printInfo.imei.toLocal8Bit().data());
    qDebug()<<"imei:"<<errCode;

    //sn code
    errCode = PTK_DrawTextTrueTypeW(178, 133, 30, 12, "Arial", 1, 600,0, 0, 0, QString("snCode").toLocal8Bit().data(), printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"snCode:"<<errCode;


    errCode = PTK_DrawBar2D_QR(18, 12, 160, 160, 0, 6, 2, 1, 0, (QString("IMEI:")+ printInfo.imei+QString(" SN:")+ printInfo.snCode).toUtf8().data());
    qDebug()<<"draw2DQR:"<< errCode;

    return RETURN_OK;
}

int MainWindow::print_3tag_lable_ptk_type1(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    int xoffset = 12-8;
    PTK_PcxGraphicsDel(QString("*").toLocal8Bit().data());
    errCode = PTK_BmpGraphicsDownload( QString("logo").toLocal8Bit().data(), QString(".\\icon\\logo.bmp").toLocal8Bit().data(), 0);

    //#1
    errCode = PTK_DrawTextTrueTypeW(xoffset+170, 18-10, 22, 0, "Arial", 1, 600,0, 0, 0, QString("imei").toLocal8Bit().data(), printInfo.imei.toLocal8Bit().data());
    qDebug()<<"imei:"<<errCode;

    errCode = PTK_DrawTextTrueTypeW(xoffset+172, 140-10, 25, 0, "Arial", 1, 600,0, 0, 0, QString("snCode").toLocal8Bit().data(), printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"snCode:"<<errCode;

    errCode = PTK_DrawBar2D_QR(xoffset+17, 20-10, 140, 140, 0, 5, 2, 1, 0, (QString("IMEI:")+ printInfo.imei+QString(" SN:")+ printInfo.snCode).toUtf8().data());
    qDebug()<<"draw2DQR:"<< errCode;

    errCode = PTK_DrawPcxGraphics(xoffset+220-32, 44-10, QString("logo").toLocal8Bit().data());


    xoffset += 378;
    //#2
    errCode = PTK_DrawTextTrueTypeW(xoffset+170, 18-10, 22, 0, "Arial", 1, 600,0, 0, 0, QString("imei").toLocal8Bit().data(), printInfo.imei.toLocal8Bit().data());
    qDebug()<<"imei:"<<errCode;

    errCode = PTK_DrawTextTrueTypeW(xoffset+172, 140-10, 25, 0, "Arial", 1, 600,0, 0, 0, QString("snCode").toLocal8Bit().data(), printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"snCode:"<<errCode;

    errCode = PTK_DrawBar2D_QR(xoffset+17, 20-10, 140, 140, 0, 5, 2, 1, 0, (QString("IMEI:")+ printInfo.imei+QString(" SN:")+ printInfo.snCode).toUtf8().data());
    qDebug()<<"draw2DQR:"<< errCode;

    errCode = PTK_DrawPcxGraphics(xoffset+220-32, 44-10, QString("logo").toLocal8Bit().data());


    xoffset += 378;
    //#3
    errCode = PTK_DrawBarcode(xoffset+75+8, 20-12, 0, (char *)"1", 2, 2, 118, 'N', printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"PTK_SetCoordinateOrigin:"<<errCode;

    errCode = PTK_DrawTextTrueTypeW(xoffset+87+8, 142-12, 30, 0, "Arial", 1, 600,0, 0, 0, QString("snCode").toLocal8Bit().data(), printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"snCode:"<<errCode;

    return RETURN_OK;
}

/*美团标签*/  // 275 157
int MainWindow::print_1tag_lable_tsc_meituan(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    LOG_DEBUG<<"imei/MID"<<printInfo.imei;
    LOG_DEBUG<<"snCode/DID"<<printInfo.snCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    // 设备类型
    QString name =  "   " + printInfo.selectType + " ";;
    errCode = tsc_windowsfont(121, 5, 30, 0, 2, 0, "Arial", name.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("BAR 121, 33, 152, 4");//140-8
    qDebug()<<"draw line:"<<errCode;

//    errCode = tsc_windowsfont(138, 5, 30, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
//    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    // 型号：AT-MT1
    QString model_type = "型号:"+printInfo.deviceModel;
    errCode = tsc_windowsfont(119, 40, 23,0,0,0,"Arial", model_type.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,model_type:"<<errCode;

    // 输入:12~80V DC
    QString input = "规格:12~80V DC 15mA";
    errCode = tsc_windowsfont(119, 65, 17,0,0,0,"Arial", input.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,input:"<<errCode;


//    errCode = tsc_windowsfont(150,65,33,0,0,0,"Arial", printInfo.selectType.toLocal8Bit().data());//73-8
//    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;


//25-4  mid 137.5  11 宽度
    errCode = tsc_windowsfont(124, 90, 20, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    QString xiaoan = "制造商: 武汉小安科技有限公司";
    errCode = tsc_windowsfont(15, 128, 20, 0, 2, 0, "Arial", xiaoan.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    QString data = QString("QRCODE 10,8,M,5,A,0,M2,\"") + printInfo.imei + QString("\""); // 打印中间码到二维码

//    QString data = QString("QRCODE 12,3,M,5,A,0,M2,\"") + printInfo.imei + QString("\""); // 打印中间码到二维码
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}
int MainWindow:: print_1tag_lable_tsc_meituan_add(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    LOG_DEBUG<<"imei/MID"<<printInfo.imei;
    LOG_DEBUG<<"snCode/DID"<<printInfo.snCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    // 设备类型 AT-MT1
    QString name = "     " + printInfo.deviceModel;
    errCode = tsc_windowsfont(111, 0, 30, 0, 2, 0, "Arial", name.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("BAR 121, 33, 152, 4");//140-8
    qDebug()<<"draw line:"<<errCode;


//    errCode = tsc_sendcommand("PUTBMP 109,40,\"icon/logo.bmp\""); //"PUTBMP 136,32,\"logo.BMP\""
    errCode = tsc_sendcommand("PUTBMP 154,48,\"logo.BMP\""); //45
    qDebug()<<"PUTBMP:"<<errCode;


//25-4  mid 137.5  11 宽度
    // imei -> mid
    errCode = tsc_windowsfont(16, 112, 30, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    QString data = QString("QRCODE 10,3,M,5,A,0,M2,\"") + printInfo.imei + QString("\""); // 打印中间码到二维码

//    QString data = QString("QRCODE 12,3,M,5,A,0,M2,\"") + printInfo.imei + QString("\""); // 打印中间码到二维码
    tsc_sendcommand(data.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}

  // 240 120  30*15 的样式二 : 蓝牙钥匙
  int MainWindow::print_1tag_lable_tsc_type8(ST_PRINT_INFO &printInfo)
  {
      int errCode = 0;

      errCode = tsc_clearbuffer();
      qDebug()<<"tsc_clearbuffer:"<<errCode;

      errCode = tsc_sendcommand("REFERENCE 0,8");
      qDebug()<<"cmd_REFERENCE:"<<errCode;


      // 设备类型 K920
      QString name =  printInfo.deviceModel;
      errCode = tsc_windowsfont(140, 8, 30, 0, 2, 0, "Arial", name.toLocal8Bit().data());//150-12
      qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

      errCode = tsc_sendcommand("BAR 109, 35, 120, 4");//140-8
      qDebug()<<"draw line:"<<errCode;

      errCode = tsc_windowsfont(109, 92, 20, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
      qDebug()<<"printerfont,imei:"<<errCode;

      errCode = tsc_sendcommand("PUTBMP 136,38,\"key.BMP\"");
      qDebug()<<"PUTBMP:"<<errCode;

      //A 自动模式   M手动模式
      //手动模式下 第一个字母用于设置数据格式 所以 A+MAC
      QString data = QString("QRCODE 12,16,M,4,M,0,M2,\"AMAC:") + printInfo.imei + QString("\"");

      tsc_sendcommand(data.toUtf8().data() );
      qDebug()<<"cmd_QRCODE"<<errCode;

      return RETURN_OK;
  }

  // 240 120  30*15 的样式三
  int MainWindow::print_1tag_lable_tsc_type9(ST_PRINT_INFO &printInfo)
  {
      int errCode = 0;

      errCode = tsc_clearbuffer();
      qDebug()<<"tsc_clearbuffer:"<<errCode;

      errCode = tsc_sendcommand("REFERENCE 0,8");
      qDebug()<<"cmd_REFERENCE:"<<errCode;

      // IMEI
      errCode = tsc_windowsfont(120, 8, 16, 0, 2, 0, "Arial", printInfo.imei.toLocal8Bit().data());
      qDebug()<<"printerfont,imei:"<<errCode;

      // 设备类型 AT-XXX  使自动对齐  二维码为4号，则边沿在 105 左右
      QString name =  printInfo.deviceModel;
      quint8 size = (name.length()<7)? 30 : 24;
      quint8 start ;  // 注意除法取整
      if(IsChinese(name)) // 中文大小为方形
      {
          size = (name.length() < 5)? 28 : 24;
          start = (240 + 120)/2.0  - name.length() * size/2.0;
      }
      else  // ascii 的宽度为高度一半
      {
          start = (240 + 105)/2.0  - (name.length() / 2.0) * size/2.0;
      }
      LOG_DEBUG <<"LEN:"<<name.length()<<"start:"<< start << "end:"<< start + name.length() * 30 /2;
      errCode = tsc_windowsfont( start, 60 - size / 2.0, size, 0, 2, 0, "Arial", name.toLocal8Bit().data());//150-12
      qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

      // SN
      errCode = tsc_windowsfont(120, 90, 20,0,2,0,"Arial", printInfo.snCode.toLocal8Bit().data());
      qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

      // QRCODE
      QString data = QString("QRCODE 12,8,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
      tsc_sendcommand(data.toUtf8().data() );
      qDebug()<<"cmd_QRCODE"<<errCode;

      return RETURN_OK;
  }

// 433钥匙  240 120  30*15 的样式四
int MainWindow::print_1tag_lable_tsc_type10(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;
//    int deviceModelx, deviceModely;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("REFERENCE 0,5");
    qDebug()<<"cmd_REFERENCE:"<<errCode;


    // 设备类型 K901
    QString name =  printInfo.deviceModel;
    quint8 size = (name.length()<7)? 30 : 24;
    quint8 start_x ;  // 注意除法取整
    quint8 offset_y;
    if(IsChinese(name)) // 中文大小为方形
    {
        size = (name.length() < 5)? 28 : 24;
        start_x = (235 + 105)/2.0  - name.length() * size/2.0;
        offset_y = (name.length() < 5)? 0 : 4;
    }
    else  // ascii 的宽度为高度一半
    {
        start_x = (235 + 105)/2.0  - (name.length() / 2.0) * size/2.0;
        offset_y = (name.length()<7)? 0 : 6;
    }
    LOG_DEBUG <<"LEN:"<<name.length()<<"start:"<< start_x << "end:"<< start_x + name.length() * 30 /2;
    errCode = tsc_windowsfont( start_x, 23 + offset_y, size, 0, 2, 0, "Arial", name.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("BAR 105, 50, 125, 4");//140-8
    qDebug()<<"draw line:"<<errCode;


    QString SpliceSN;
    quint8 size_sn = 21;
    SpliceSN = printInfo.snCode.rightJustified(6,'0') + printInfo.imei.rightJustified(6,'0');
    errCode = tsc_windowsfont(105, 60 - size / 4.0 + 15, size_sn, 0, 2, 0, "Arial", SpliceSN.toLocal8Bit().data());
    qDebug()<<"printerfont,snCode:"<<errCode;

    errCode = tsc_sendcommand("PUTBMP 5,18,\"keyBIG.BMP\""); // 85
    qDebug()<<"PUTBMP:"<<errCode;

    return RETURN_OK;
}

// 280 * 160   35 * 20  样式一
//type1:打印设备类型、型号、imei、sn、QRcode用于小蜜芒果等热敏设备标签
int MainWindow::print_1tag_lable_tsc_type11(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("REFERENCE 0,8");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    quint8 size_model = (printInfo.deviceModel.length()<8)? 30 : 24;
    quint8 start_model ;  // 注意除法取整
    quint8 offsety_model;
    if(IsChinese(printInfo.deviceModel)) // 中文大小为方形
    {
        size_model = (printInfo.deviceModel.length() < 5)? 28 : 24;
        start_model = (280 + 120)/2.0  - printInfo.deviceModel.length() * size_model/2.0;
        offsety_model = (printInfo.deviceModel.length() < 5)? 0 : 4;
    }
    else  // ascii 的宽度为高度一半
    {
        start_model = (280 + 105)/2.0  - (printInfo.deviceModel.length() / 2.0) * size_model/2.0;
        offsety_model = (printInfo.deviceModel.length() < 8)? 0 : 7;
    }
    errCode = tsc_windowsfont(start_model, 5 + offsety_model, size_model, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("DIAGONAL 124, 33, 260, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    // 设备类型
    QString name_type =  printInfo.selectType;
    quint8 size_type = (name_type.length() < 8)? 33:24;
    quint8 start_tpye ; // 注意除法取整
    quint8 offsety_type;
    if(IsChinese(name_type)) // 中文大小为方形
    {
        size_type = (name_type.length() < 5)? 33:26;
        start_tpye = (280 + 120)/2.0  - name_type.length() * size_type/2.0;
        offsety_type = (name_type.length() < 5)? 0:4;
    }
    else  // ascii 的宽度为高度一半 '-' 半宽
    {
        start_tpye = (280 + 120)/2.0  - (name_type.length() / 2.0) * size_type/2.0;
        offsety_type = (name_type.length() < 8)? 0:4;
        start_tpye -= (name_type.length() < 8)? 0:size_type/2.0;
    }
    errCode = tsc_windowsfont( start_tpye, 80 - size_type/2.0 + offsety_type, size_type,0,0,0, "Arial", name_type.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    QString qrCodeData;
    qrCodeData = QString("QRCODE 12,5,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(qrCodeData.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;
    return RETURN_OK;
}

//type7:双排35*20mm亚银标签纸 280 * 160
int MainWindow::print_2tag_lable_tsc_type2(ST_PRINT_INFO &printInfo)
{
    int errCode = 0;
    QString qrCodeData;
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

//    errCode = tsc_sendcommand("OFFSET 0 dot");
//    qDebug()<<"cmd_OFFSET:"<<errCode;

    //打印内容1 reference = 2mm = 16dot
    errCode = tsc_sendcommand("REFERENCE 24,8");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    quint8 size_model = (printInfo.deviceModel.length()<8)? 30 : 24;
    quint8 start_model ;  // 注意除法取整
    quint8 offsety_model;
    if(IsChinese(printInfo.deviceModel)) // 中文大小为方形
    {
        size_model = (printInfo.deviceModel.length() < 5)? 28 : 24;
        start_model = (280 + 120)/2.0  - printInfo.deviceModel.length() * size_model/2.0;
        offsety_model = (printInfo.deviceModel.length() < 5)? 0 : 4;
    }
    else  // ascii 的宽度为高度一半
    {
        start_model = (280 + 105)/2.0  - (printInfo.deviceModel.length() / 2.0) * size_model/2.0;
        offsety_model = (printInfo.deviceModel.length() < 8)? 0 : 7;
    }
    errCode = tsc_windowsfont(start_model, 5 + offsety_model, size_model, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("DIAGONAL 124, 33, 260, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    // 设备类型
    QString name_type =  printInfo.selectType;
    quint8 size_type = (name_type.length() < 8)? 33:24;
    quint8 start_tpye ; // 注意除法取整
    quint8 offsety_type;
    if(IsChinese(name_type)) // 中文大小为方形
    {
        size_type = (name_type.length() < 5)? 33:26;
        start_tpye = (280 + 120)/2.0  - name_type.length() * size_type/2.0;
        offsety_type = (name_type.length() < 5)? 0:4;
    }
    else  // ascii 的宽度为高度一半 '-' 半宽
    {
        start_tpye = (280 + 120)/2.0  - (name_type.length() / 2.0) * size_type/2.0;
        offsety_type = (name_type.length() < 8)? 0:4;
        start_tpye -= (name_type.length() < 8)? 0:size_type/2.0;
    }
    errCode = tsc_windowsfont( start_tpye, 80 - size_type/2.0 + offsety_type, size_type,0,0,0, "Arial", name_type.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    qrCodeData = QString("QRCODE 12,5,M,4,A,0,M2,\"IMEI:") + printInfo.imei + QString(" SN:") + printInfo.snCode + QString("\"");
    tsc_sendcommand(qrCodeData.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    //打印内容2 reference: 2mm+35mm+2mm = 39mm = 312
    errCode = tsc_sendcommand("REFERENCE 320,8");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    errCode = tsc_windowsfont(start_model, 5 + offsety_model, size_model, 0, 2, 0, "Arial", printInfo.deviceModel.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw model name:"<< errCode;

    errCode = tsc_sendcommand("DIAGONAL 124, 33, 260, 33 , 3");//140-8
    qDebug()<<"draw line:"<<errCode;

    errCode = tsc_windowsfont(125, 37, 22,0,0,0,"Arial", printInfo.snCode.toLocal8Bit().data());
    qDebug()<<"tsc_windowsfont,snCode:"<<errCode;

    errCode = tsc_windowsfont( start_tpye, 80 - size_type/2.0 + offsety_type, size_type,0,0,0, "Arial", name_type.toLocal8Bit().data());//150-12
    qDebug()<<"tsc_windowsfont,draw logo txt:"<<errCode;

    errCode = tsc_windowsfont(25, 112, 30, 0, 0, 0, "Arial", printInfo.imei.toLocal8Bit().data());
    qDebug()<<"printerfont,imei:"<<errCode;

    tsc_sendcommand(qrCodeData.toUtf8().data() );
    qDebug()<<"cmd_QRCODE"<<errCode;

    return RETURN_OK;
}











///////////////////////////////
int MainWindow::print_lable_tsc(ST_PRINT_INFO &printInfo, int printTimes)
{
    int errCode = 0;
    QSettings settings("xiaoanTech", "Printer");
    settings.beginGroup("MainWindow");

    //config printer
    errCode = tsc_openport("TSC TTP-244 Pro");
    qDebug()<<"tsc_openport:"<<errCode;
    if(errCode != 1)
    {
        return PRINTER_NOT_OPEN;
    }

    errCode = tsc_clearbuffer();
    qDebug()<<"tsc_clearbuffer:"<<errCode;

    errCode = tsc_sendcommand("DIRECTION 1,0");
    qDebug()<<"cmd_DIRECTION:"<<errCode;
    errCode = tsc_sendcommand("REFERENCE 0,0");
    qDebug()<<"cmd_REFERENCE:"<<errCode;

    // 30 * 15
    if(this->setting_type.label < TYPE_1TAG)
    {
        errCode = tsc_sendcommand("SIZE 240 dot,120 dot");
        qDebug()<<"cmd_SIZE:"<<errCode;

        errCode = tsc_sendcommand("GAP 2 mm,0 mm");
        qDebug()<<"cmd_GAP:"<<errCode;

        tsc_sendcommand("SPEED 1");
        qDebug()<<"cmd_SPEED:"<<errCode;

    }
    else if(this->setting_type.label < TYPE_1TAG_SILVER_40_30)
    {   // 35 * 20
        errCode = tsc_sendcommand("SIZE 275 dot,157 dot");
        qDebug()<<"cmd_SIZE:"<<errCode;

        errCode = tsc_sendcommand("GAP 2.67 mm,0 mm");
        qDebug()<<"cmd_GAP:"<<errCode;

        tsc_sendcommand("SPEED 6");
        qDebug()<<"cmd_SPEED:"<<errCode;
    }
    else if(this->setting_type.label < TYPE_3TAG)
    {// 40 * 30
        errCode = tsc_sendcommand("SIZE 320 dot,240 dot");
        qDebug()<<"cmd_SIZE:"<<errCode;

        errCode = tsc_sendcommand("GAP 2 mm,0 mm");
        qDebug()<<"cmd_GAP:"<<errCode;

        tsc_sendcommand("SPEED 2");
        qDebug()<<"cmd_SPEED:"<<errCode;
    }
    else if(this->setting_type.label < TYPE_2TAG)
    {
        //设置纸张 96mm * 17mm
        errCode = tsc_sendcommand("SIZE 96 mm,15.2 mm");
        qDebug()<<"cmd_SIZE:"<<errCode;

        errCode = tsc_sendcommand("GAP 2 mm,0 mm");
        qDebug()<<"cmd_GAP:"<<errCode;

        tsc_sendcommand("SPEED 1");
        qDebug()<<"cmd_SPEED:"<<errCode;
    }
    else if(this->setting_type.label < TYPE_LABEL_NONE)
    {
        //设置纸张双排35*20亚银标签 纸带宽76mm 76*8 =608 dot
        errCode = tsc_sendcommand("SIZE 76 mm,20 mm");
        qDebug()<<"cmd_SIZE:"<<errCode;

        errCode = tsc_sendcommand("GAP 2 mm,0 mm");
        qDebug()<<"cmd_GAP:"<<errCode;

        tsc_sendcommand("SPEED 2");
        qDebug()<<"cmd_SPEED:"<<errCode;
    }

    errCode = tsc_sendcommand("OFFSET 0 dot");
    qDebug()<<"cmd_OFFSET:"<<errCode;

    errCode = tsc_sendcommand("DENSITY 15");
    qDebug()<<"cmd_DENSITY:"<<errCode;

    //set info
    switch(this->setting_type.label)
    {
        case TYPE_1TAG_TYPE1:
            qDebug()<< "label 1tag type 1";
            print_1tag_lable_tsc_type1(printInfo);
            break;
        case TYPE_1TAG_TYPE5:
            qDebug()<< "label 1tag type 5";
            print_1tag_lable_tsc_type5(printInfo);
            break;

        case TYPE_1TAG_TYPE6:
            qDebug()<< "label 1tag type 6";
            print_1tag_lable_tsc_type6(printInfo);
            break;

        case TYPE_1TAG_TYPE2:
        case TYPE_1TAG_TYPE3:
            qDebug()<< "label 1tag type 2/3";
            print_1tag_lable_tsc_type2(printInfo);
            break;
        case TYPE_1TAG_TYPE4:
            printTimes = printTimes*2;
            qDebug()<< "label 1tag type 4";
            print_1tag_lable_tsc_type2(printInfo);
            break;

        case TYPE_1TAG_TYPE7:
            qDebug()<< "label 1tag type 7";
            print_1tag_lable_tsc_type7(printInfo);
            break;

        case TYPE_3TAG_TYPE1:
            qDebug()<< "label 3tag type 1";
            print_3tag_lable_tsc_type1(printInfo);
            break;
        case TYPE_2TAG_TYPE1:
            qDebug()<< "label 2tag type 1";
            print_2tag_lable_tsc_type1(printInfo);
            break;
        case TYPE_MEITUAN:
            qDebug()<< "label 2tag type 1";
            print_1tag_lable_tsc_meituan(printInfo);
            break;

        case TYPE_1TAG_TYPE8:
            qDebug()<< "label 1tag type 8";
            print_1tag_lable_tsc_type8(printInfo);
            break;

        case TYPE_1TAG_TYPE9:
            qDebug()<< "label 1tag type 9";
            print_1tag_lable_tsc_type9(printInfo);
            break;

        case TYPE_1TAG_TYPE10:
            qDebug()<< "label 1tag type 10";
            print_1tag_lable_tsc_type10(printInfo);
            break;

        case TYPE_1TAG_TYPE11:
            qDebug()<< "label 1tag type 11";
            print_1tag_lable_tsc_type11(printInfo);
            break;

        case TYPE_2TAG_TYPE2:
            qDebug()<< "label 2tag type 2";
            print_2tag_lable_tsc_type2(printInfo);
            break;

        default:
            qDebug()<< "tag type not found";
            errCode = tsc_closeport();
            qDebug()<<"tsc_closeport()"<<errCode;
            return PRINTER_NOT_OPEN;
    }

    //print times

    errCode = tsc_printlabel("1",QString::number(printTimes,10).toLatin1());
    qDebug()<<"print tag " <<errCode;

    //patch
    if(this->setting_type.label == TYPE_1TAG_TYPE3)
    {
        setting_type.label = TYPE_1TAG_TYPE2;
        qDebug()<< "label 1tag type 3";
        print_1tag_lable_tsc_type3(printInfo);
        errCode = tsc_printlabel("1",QString::number(printTimes,10).toLatin1());
        qDebug()<<"print tag "<<errCode;
    }

    if(this->setting_type.label == TYPE_1TAG_TYPE4)
    {
        qDebug()<< "label 1tag type 4";
        print_1tag_lable_tsc_type3(printInfo);
        errCode = tsc_printlabel("1",QString::number(printTimes/2,10).toLatin1());
        qDebug()<<"print tag "<<errCode;
    }

    if(this->setting_type.label == TYPE_MEITUAN)
    {
        qDebug()<< "label 2tag type 1";
        print_1tag_lable_tsc_meituan_add(printInfo);
        errCode = tsc_printlabel("1",QString::number(printTimes*2,10).toLatin1());
        qDebug()<<"print tag "<<errCode;
    }

    //close
    errCode = tsc_closeport();
    qDebug()<<"tsc_closeport()"<<errCode;

    return RETURN_OK;

}

int MainWindow::print_lable_ptk(ST_PRINT_INFO &printInfo, int printTimes)
{
        int errCode = 0;

        errCode = openUsbPort(255);
        qDebug()<<"openUsbPort:"<<errCode;
        if(errCode != 0)
        {
            return PRINTER_NOT_OPEN;
        }
        errCode = PTK_ClearBuffer();
        qDebug()<<"PTK_ClearBuffer:"<<errCode;

        errCode = PTK_SetDirection('B');
        qDebug()<<"PTK_SetDirection:"<<errCode;

        if(this->setting_type.label < TYPE_3TAG)
        {
            errCode = PTK_SetLabelHeight(177, 24, 0 , false);
            qDebug()<<"PTK_SetLabelHeight:"<<errCode;

            errCode = PTK_SetLabelWidth(1134);
            qDebug()<<"PTK_SetLabelWidth:"<<errCode;
        }
        else if(this->setting_type.label < TYPE_LABEL_NONE)
        {
            errCode = PTK_SetLabelHeight(177, 24, 0 , false);
            qDebug()<<"PTK_SetLabelHeight:"<<errCode;

            errCode = PTK_SetLabelWidth(1134);
            qDebug()<<"PTK_SetLabelWidth:"<<errCode;
        }

        errCode = PTK_SetDarkness(20);
        qDebug()<<"PTK_SetDarkness:"<<errCode;

        errCode = PTK_SetPrintSpeed(3);
        qDebug()<<"PTK_SetPrintSpeed:"<<errCode;

        QSettings settings("xiaoanTech", "Printer");
        settings.beginGroup("MainWindow");

        switch(this->setting_type.label)
        {
//            case TYPE_1TAG_TYPE1:
//                 print_1tag_lable_ptk_type1(printInfo);
//                 break;
            case TYPE_3TAG_TYPE1:
                print_3tag_lable_ptk_type1(printInfo);
                break;
            default:
                QMessageBox::critical(NULL, ("error"), ("not support print type"), QMessageBox::Yes, QMessageBox::Yes);
                errCode = closeUsbPort();
                qDebug()<<"closeUsbPort()"<<errCode;
                return PRINTER_NOT_OPEN;
        }

        errCode  = PTK_PrintLabel(1, printTimes);
        qDebug()<<"print tag "<< errCode;

        errCode = PTK_GetErrState();
        qDebug()<<"get err State"<<errCode;

        errCode = closeUsbPort();
        qDebug()<<"closeUsbPort()"<<errCode;

        return RETURN_OK;
}

