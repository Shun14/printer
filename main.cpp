#include "mainwindow.h"
#include "startdialog.h"
#include <QApplication>
#include <QtDebug>
#include <QMessageBox>

#include "data.h"

#define Title "  小安科技-标签生成工具  "
#define Version " V20191029 "

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    StartDialog dlg;

    w.setting_type.label = TYPE_LABEL_NONE;
    if(dlg.exec() == QDialog::Accepted)
    {
        w.setting_type.printer = dlg.setting_type.printer;
        w.setting_type.label = dlg.setting_type.label;

        if(w.setting_type.label == TYPE_LABEL_NONE)
        {
            QMessageBox::critical(NULL, ("error"), ("tag type error"), QMessageBox::Yes, QMessageBox::Yes);
            return 0;
        }

        switch(w.setting_type.printer)
        {
            case TYPE_TSC:
                qDebug()<< "TTP244PRO";
                break;
            case TYPE_PTK:
                qDebug()<< "POSTEK";
                break;
            default:
                return RETURN_LOADDLLERROR;
        }

        if(RETURN_OK != w.InitDll())
        {
            QMessageBox::critical(NULL, ("error"), ("dll init error"), QMessageBox::Yes, QMessageBox::Yes);
            return 0;
        }
        w.ReconfigUart();
        w.ReconfigUI();

        {// 配置主窗口名字
            QString WinName = Title;
            QString version = Version;
            QString WinTitle;
            WinTitle = WinName + version + "-> \"" + dlg.UserTagName + "\"";
            qDebug() <<  dlg.UserTagName;
            w.setWindowTitle(WinTitle);
        }

        w.show();
        return a.exec();
    }
    else
    {
        return 0;
    }
}
