#ifndef EXPAND_H
#define EXPAND_H

#define htonl(l) ((((l >> 24) & 0x000000ff)) | \
                  (((l >>  8) & 0x0000ff00)) | \
                  (((l) & 0x0000ff00) <<  8) | \
                  (((l) & 0x000000ff) << 24))
#define ntohl(l) htonl(l)

#define htons(s) ((((s) >> 8) & 0xff) | \
                  (((s) << 8) & 0xff00))
#define ntohs(s) htons(s)

#define SIGNATURE_485 0xaa55


typedef enum{
    ADR_BLE_KEY     = 0x02, // 蓝牙钥匙地址

} ADDRESS_485;


typedef enum
{
    CMD_BLE_KEY_MAC             = 0x3C, // 获取蓝牙钥匙 MAC 地址

    CMD_NUM,
} CMD_EXP;





#pragma pack(push, 1)

typedef struct{
    quint16 signature;
    quint8 address;
    quint8 cmd;
    quint8 length;
    quint8 data[];
}__attribute__((__packed__)) HEADER;

typedef struct
{
    quint8 version;
}__attribute__((__packed__))BLE_KEY_MAC_MSG;

typedef struct
{
    quint8 version[6];
}__attribute__((__packed__))BLE_KEY_MAC_MSG_RSP;

typedef qint8 (*STM_MSG_PROC)(HEADER *msg);



#pragma pack(pop)


#endif // EXPAND_H
