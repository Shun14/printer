#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLibrary>
#include <QSettings>

#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QtSerialPort>

#include "data.h"
#include "expand.h"

namespace Ui
{
class MainWindow;
}
#define MAX_UART_NUM  (5)
#define RETURN_OK 0              // Return OK
#define RETURN_LOADDLLERROR 3001 // Loaddll Error
#define PRINTER_NOT_OPEN -1060
#define IMEI_NOT_FOUND 101
#define SNCODE_LENGTH 12


typedef char *NPSTR, *LPTSTR, *PSTR;
typedef const char *LPCSTR, *PCSTR;
typedef char TCHAR;
typedef LPCSTR PCTSTR, LPCTSTR, PCUTSTR, LPCUTSTR;

typedef int(__stdcall *typ_about)(void);
typedef int(__stdcall *typ_openport)(LPCSTR printername);
typedef int(__stdcall *typ_barcode)(LPCSTR x,		LPCSTR y,		LPCSTR type,
                                    LPCSTR height,	LPCSTR readable,	LPCSTR rotation,
                                    LPCSTR narrow,	LPCSTR wide,		LPCSTR code);
typedef int(__stdcall *typ_clearbuffer)(void);
typedef int(__stdcall *typ_closeport)(void);
typedef int(__stdcall *typ_downloadpcx)(void);
typedef int(__stdcall *typ_formfeed)(LPCSTR filename,LPCSTR image_name);
typedef int(__stdcall *typ_nobackfeed)(void);
typedef int(__stdcall *typ_printerfont)(LPCSTR x,			LPCSTR y,		LPCSTR fonttype,
                                       LPCSTR rotation,		LPCSTR xmul,		LPCSTR ymul,
                                       LPCSTR text);
typedef int(__stdcall *typ_printlabel)(LPCSTR set, LPCSTR copy);
typedef int(__stdcall *typ_sendcommand)(LPCSTR printercommand);
typedef int(__stdcall *typ_setup)(LPCSTR width,	LPCSTR height,
                                  LPCSTR speed,	LPCSTR density,
                                  LPCSTR sensor,	LPCSTR vertical,
                                  LPCSTR offset);
typedef int(__stdcall *typ_windowsfont)(int x,				int y,			int fontheight,
                                        int rotation,		int fontstyle,	int fontunderline,
                                        LPCSTR szFaceName,	LPCSTR content);

typedef int(__stdcall *fOpenUSBPort)(unsigned int nUSBID);
typedef int(__stdcall *fCloseUSBPort)(void);
typedef int(__stdcall *fPTK_GetErrState)(void);
typedef int(__stdcall *fPTK_ClearBuffer)();
typedef int(__stdcall *fPTK_SetDirection)(char direct);
typedef int(__stdcall *fPTK_SetLabelHeight)(unsigned int lheight, unsigned int gapH, int gapOffset, bool bFlag);
typedef int(__stdcall *fPTK_SetLabelWidth)(unsigned int lwidth);
typedef int(__stdcall *fPTK_SetDarkness)(unsigned int id);
typedef int(__stdcall *fPTK_SetPrintSpeed)(unsigned int px);
typedef int(__stdcall *fPTK_DrawRectangle)(unsigned int px, unsigned int py,unsigned int thickness, unsigned int pEx,unsigned int pEy);
typedef int(__stdcall *fPTK_DrawText)(unsigned int px, unsigned int py,unsigned int pdirec, unsigned int pFont,unsigned int pHorizontal,unsigned int pVertical,char ptext, LPTSTR pstr);

typedef int(__stdcall *fPTK_DrawBar2D_QR)(unsigned int x, unsigned int y,unsigned int w, unsigned int v,unsigned int o, unsigned int r,unsigned int m, unsigned int g,unsigned int s, LPTSTR pstr);

typedef int(__stdcall *fPTK_PrintLabel)(unsigned int number, unsigned int cpnumber);
typedef int(__stdcall *fPTK_PcxGraphicsDownload)(char *pcxname, char *pcxpath);
typedef int(__stdcall *fPTK_BmpGraphicsDownload)(LPTSTR pcxname, LPTSTR pcxpath, int iDire);
typedef int(__stdcall *fPTK_DrawPcxGraphics)(unsigned int px, unsigned int py, LPTSTR gname);
typedef int (__stdcall *fPTK_DrawTextTrueTypeW)(int x,int y,unsigned  int FHeight,
                                   unsigned  int FWidth, LPCTSTR FType,
                                   unsigned  int Fspin,unsigned  int FWeight,bool FItalic,bool FUnline,
                                   bool FStrikeOut,LPTSTR id_name,LPCTSTR data);
typedef int (__stdcall *fPTK_DrawLineOr)(unsigned int px,unsigned int py,
                            unsigned int plength,unsigned int pH);
typedef int (__stdcall *fPTK_SetCoordinateOrigin)(unsigned int px, unsigned int py);

typedef int (__stdcall *fPTK_DrawBarcode)(unsigned int px, unsigned int py, unsigned int pdirec, LPTSTR pcode,
                                                  unsigned int NarrowWith, unsigned int pHorizontal, unsigned int pVertical, TCHAR ptext, LPTSTR pstr);
typedef int (__stdcall *fPTK_PcxGraphicsDel)(LPTSTR pid);
typedef int (__stdcall *fPTK_PrintPCX)(unsigned int px, unsigned int py,LPTSTR filename);

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = 0);
    QLibrary loadLib;
    ~MainWindow();

    ST_TYPE_SETTING setting_type;

  public:
    Ui::MainWindow *ui;
    int snCodeSuffix;
    int transactionCount;
    int InitDll();
    int ReconfigUart(void);
    int ReconfigUI(void);

    typ_about tsc_about = NULL;
    typ_openport tsc_openport = NULL;
    typ_barcode tsc_barcode = NULL;
    typ_clearbuffer tsc_clearbuffer = NULL;
    typ_closeport tsc_closeport = NULL;
    typ_downloadpcx tsc_downloadpcx = NULL;
    typ_formfeed tsc_formfeed = NULL;
    typ_nobackfeed tsc_nobackfeed = NULL;
    typ_printerfont tsc_printerfont = NULL;
    typ_printlabel tsc_printlabel = NULL;
    typ_sendcommand tsc_sendcommand = NULL;
    typ_setup tsc_setup = NULL;
    typ_windowsfont tsc_windowsfont = NULL;


    fOpenUSBPort openUsbPort = NULL;
    fCloseUSBPort closeUsbPort = NULL;
    fPTK_GetErrState PTK_GetErrState = NULL;
    fPTK_ClearBuffer PTK_ClearBuffer = NULL;
    fPTK_SetDirection PTK_SetDirection = NULL;
    fPTK_DrawBar2D_QR PTK_DrawBar2D_QR = NULL;
    fPTK_SetLabelHeight PTK_SetLabelHeight = NULL;
    fPTK_SetLabelWidth PTK_SetLabelWidth = NULL;
    fPTK_SetDarkness PTK_SetDarkness = NULL;
    fPTK_SetPrintSpeed PTK_SetPrintSpeed = NULL;
    fPTK_DrawRectangle PTK_DrawRectangle = NULL;
    fPTK_DrawText PTK_DrawText = NULL;
    fPTK_PrintLabel PTK_PrintLabel = NULL;
    fPTK_PcxGraphicsDownload PTK_PcxGraphicsDownload = NULL;
    fPTK_BmpGraphicsDownload PTK_BmpGraphicsDownload = NULL;
    fPTK_DrawPcxGraphics PTK_DrawPcxGraphics = NULL;
    fPTK_DrawTextTrueTypeW PTK_DrawTextTrueTypeW = NULL;
    fPTK_DrawLineOr PTK_DrawLineOr = NULL;
    fPTK_SetCoordinateOrigin PTK_SetCoordinateOrigin = NULL;
    fPTK_DrawBarcode PTK_DrawBarcode = NULL;
    fPTK_PcxGraphicsDel PTK_PcxGraphicsDel = NULL;
    fPTK_PrintPCX PTK_PrintPCX = NULL;

    QSettings selfSettings;
    QString txtFileName;
    QStringList imeiList;
    QStringList snCodeList;

    QStringList imeiMixList;
    QStringList typeList;
    QStringList modeList;


    QString single_txtFileName;
    QStringList single_imeiList;
    QStringList single_snCodeList;

public:
    // expand
    //uart
    void uart_lsPort(void);
    quint8 uart_openPort(void);
    quint8 uart_openPort(QString COM_name);
    quint8 uart_closePort(void);
    qint8  uart_init(void);

    qint8 uart_msg_proc(HEADER *msg);
    qint8  uart_ble_key_mac_rsp(HEADER *msg);
    qint8 ble_key_send_pkg(uchar cmd, uchar *data, quint16 len);

    // util
    bool isDigitString(const QString& src);

private slots:
    void uart_receiveInfo();
    void ble_key_ping(void);

private slots:
  void on_getImeiButton_clicked();
  void on_getImeiButton_getImei_clicked();
  void on_getTxtPushButton_batch_clicked();


  int createSingleLabel(QString imei, int index);

  void showResponse(const QString &str);
  void processError(const QString &str);
  void processTimeout(const QString &str);

  void addLog(QString str);
  void on_exportLabel_single_clicked();
  void on_exportLabel_batch_clicked();

  void on_single_pushButton_choseFile_clicked();

//serial port
private slots:
  void sendData();
  void readData();

  void on_serialPortComboBox_single_activated(const QString &arg1);

private:
  // uart
  QSerialPort *serial = NULL;
  QSerialPort *pCOM;
  QSerialPort *pCOM_USR[MAX_UART_NUM]; // 用于后期拓展
  QStringList COM_list;
  bool portisOpen;

  // timer
  QTimer *timer;
  QTimer *timer_next;

  // expand
  bool flg_getMAC;

  bool readCfgJson();

  int init_dll_tsc();
  int init_dll_ptk();

  //200DPI 1mm=8point 1point=0.125mm
  //300DPI 1mm=12point

  int print_lable_tsc(ST_PRINT_INFO &printInfo, int printTimes);//新打印机 200DPI
  int print_lable_ptk(ST_PRINT_INFO &printInfo, int printTimes);

  int print_1tag_lable_tsc_type1(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type2(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type3(ST_PRINT_INFO &printInfo);
  int print_3tag_lable_tsc_type1(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type5(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type6(ST_PRINT_INFO &printInfo);
  int print_2tag_lable_tsc_type1(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_meituan(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_meituan_add(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type7(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type8(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type9(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type10(ST_PRINT_INFO &printInfo);
  int print_1tag_lable_tsc_type11(ST_PRINT_INFO &printInfo);

  int print_2tag_lable_tsc_type2(ST_PRINT_INFO &printInfo);


  int print_1tag_lable_ptk_type1(ST_PRINT_INFO &printInfo);
  int print_3tag_lable_ptk_type1(ST_PRINT_INFO &printInfo);



  QString meituan_sn_to_middle(QString &deviceId);
};

#endif // MAINWINDOW_H

