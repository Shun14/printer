#include <QSerialPortInfo>
#include <QSerialPort>
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>
#include <QDataStream>
#include <qglobal.h>
#include <QTimer>
#include <qDebug>
#include <QLabel>
#include <QFile>
#include <QDir>

#include <QCoreApplication>
#include <qfiledialog.h>
#include <QMessageBox>
#include <QJsonObject>
#include <QKeyEvent>
#include <QDateTime>
#include <QLibrary>
#include <QTabBar>
#include <QDebug>
#include <QTime>
#include <QFile>

#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "data.h"

#define LOG_DEBUG  qDebug()<<"["<<__FILE__<<":"<<__LINE__<<__FUNCTION__<<"]"


static void hex2ascii( const quint8 hex, char *ascii )
{
    quint8 temp;
    temp        = (hex & 0xF0) >> 4;
    ascii[0]    = (temp >= 10) ? (temp - 10 + 'a') : (temp + '0');

    temp        = hex & 0x0F;
    ascii[1]    = (temp >= 10) ? (temp - 10 + 'a') : (temp + '0');
}
/*
 * *asciibuf_size >= 2 * hexbuf_len
 */
quint16 hexString2ascii(char *asciibuf, const quint8 *hexbuf, quint16 hexbuf_len)
{
    char *pascii = asciibuf;
    int i = 0;
    for (; i < hexbuf_len; i++ )
    {
        hex2ascii( hexbuf[i], pascii );
        pascii += 2;
    }
//    *pascii = '\n';
    return(hexbuf_len * 2);
}


bool checksum_pass(HEADER *header)
{
    quint8 checksum = 0;
    quint8 *pdata = &(header->address);

//    qDebug()<<">> "<<header->length;
    while((pdata - &(header->address)) != (3 + (header->length)))
    {
        checksum += *(pdata++);
    }
//    qDebug()<<">> "<<checksum<<" " << *pdata;
    return (*pdata == checksum);
}

quint8 set_check_sum(HEADER *header)
{
    quint8 checksum = 0;
    quint8 *pdata = &(header->address);

//    qDebug()<<">> "<<header->length;
    while((pdata - &(header->address)) != (3 + (header->length)))
    {
        checksum += *(pdata++);
    }
    *pdata = checksum;
//    qDebug()<<">> "<<checksum;
    return 0;
}

// LS
void MainWindow:: uart_lsPort()
{
    QStringList COM_new;
    foreach(const QSerialPortInfo &info,QSerialPortInfo::availablePorts())
    {
        COM_new << info.portName();
        LOG_DEBUG<<"LS: serialPortName:"<<info.portName();
        LOG_DEBUG<<info.description();
    }
    COM_list = COM_new;
    if(COM_list.size() == 0) portisOpen = false;
}

// CLOSE
quint8 MainWindow::uart_closePort()
{
    qDebug()<<"into close port";
    if(COM_list.size()==0)
    {
        addLog("未找到串口");
        return false;
    }
    if(pCOM->isOpen())//关闭串口
    {
        pCOM->close();
        portisOpen = false;
        qDebug()<<"close port: "<<pCOM->portName();
//        Button_red();
//        ui->portButton->setText("打开串口");
        addLog("串口已关闭");
    }
    return true;
}

// OPEN 1
quint8 MainWindow:: uart_openPort()
{
    LOG_DEBUG<<"here";
    if(COM_list.size()==0)
    {
        addLog("未找到串口");
        return false;
    }
    LOG_DEBUG<<"here";
    if(pCOM->isOpen())//如果串口已经打开了 先关闭
    {
        pCOM->clear();
        pCOM->close();
    }
    LOG_DEBUG<<"here";
    //设置串口名字
    pCOM->setPortName(COM_list[0]);
//    pCOM->setPortName("COM5");

    LOG_DEBUG<<"here";
    if(!pCOM->open(QIODevice::ReadWrite))//用ReadWrite 的模式尝试打开串口
    {
        qDebug()<<COM_list[0]<<"打开失败!";
        LOG_DEBUG<<"COM open fail";
        addLog("串口打开失败！请刷新串口后重试");
        return false;
    }
    //打开成功
    pCOM->setBaudRate(QSerialPort::Baud115200,QSerialPort::AllDirections);//设置波特率和读写方向
    pCOM->setDataBits(QSerialPort::Data8);      //数据位为8位
    pCOM->setFlowControl(QSerialPort::NoFlowControl);//无流控制
    pCOM->setParity(QSerialPort::NoParity); //无校验位
    pCOM->setStopBits(QSerialPort::OneStop); //一位停止位
    pCOM->setReadBufferSize(0);
    LOG_DEBUG<<"here";
    qDebug()<<"serialPort:"<< pCOM->portName() <<"init done 打开！";
    QObject::connect(pCOM,SIGNAL(readyRead()),this,SLOT(uart_receiveInfo()));

//    Button_green();
    portisOpen = true;
//    ui->portButton->setText("关闭串口");
    addLog("串口已打开");
    return true;
}

// OPEN 2
quint8 MainWindow:: uart_openPort(QString COM_name)
{
    if(pCOM->isOpen())//如果串口已经打开了 先关闭
    {
        pCOM->clear();
        pCOM->close();
    }

    //设置串口名字
    pCOM->setPortName(COM_name);

    if(!pCOM->open(QIODevice::ReadWrite))//用ReadWrite 的模式尝试打开串口
    {
        qDebug()<<COM_name<<"打开失败!";
        addLog("串口打开失败！");
        return false;
    }
    //打开成功
    pCOM->setBaudRate(QSerialPort::Baud9600,QSerialPort::AllDirections);//设置波特率和读写方向
    pCOM->setDataBits(QSerialPort::Data8);      //数据位为8位
    pCOM->setFlowControl(QSerialPort::NoFlowControl);//无流控制
    pCOM->setParity(QSerialPort::NoParity); //无校验位
    pCOM->setStopBits(QSerialPort::OneStop); //一位停止位
    pCOM->setReadBufferSize(0);

    qDebug()<<"serialPort:"<< pCOM->portName() <<"init done 打开！";
    QObject::connect(pCOM,SIGNAL(readyRead()),this,SLOT(uart_receiveInfo()));

//    Button_green();
    portisOpen = true;
//    ui->portButton->setText("关闭串口");
    addLog("串口已打开");
    return true;
}

// uart init scan here
qint8 MainWindow::uart_init(void)
{
    pCOM= new QSerialPort();
    portisOpen = false;

    uart_lsPort();

    LOG_DEBUG<< "after ls";

    if(setting_type.label == TYPE_1TAG_TYPE8)
    {
        uart_openPort();
    }
    else
    {
        const auto infos = QSerialPortInfo::availablePorts();
        for (const QSerialPortInfo &info : infos)
        {
            qDebug()<<info.description();
            ui->serialPortComboBox_single->addItem(info.portName()+ " " +info.description());

            LOG_DEBUG<< "HERE";

            /*临时版本*/
            if(serial == NULL)
            {
                LOG_DEBUG<< "HERE";
                addLog(("connect to " + info.portName()));
                serial = new QSerialPort;
                serial->setPortName(info.portName());  // 指定端口
                serial->setBaudRate(QSerialPort::Baud115200); // 设置波特率
                serial->setDataBits(QSerialPort::Data8); // 设置位数
                serial->setParity(QSerialPort::EvenParity); // 设置偶校验
                serial->setStopBits(QSerialPort::OneStop); // 设置一个停止位
                serial->setFlowControl(QSerialPort::NoFlowControl); // 设置无流控制
                serial->open(QIODevice::ReadWrite); // 读写模式打开串口
                connect(serial,SIGNAL(readyRead()),this,SLOT(readData())); // 连接串口的信号和槽函数
            }
        }
    }


    if((setting_type.label == TYPE_1TAG_TYPE8) && portisOpen)
    {
        timer = new QTimer(this);
        connect(timer,SIGNAL(timeout()),this,SLOT(ble_key_ping()));
        timer_next = new QTimer(this);
        connect(timer_next,SIGNAL(timeout()),this,SLOT(ble_key_ping()));
        timer->start(10);
    }
}


// read all uart data , check protocol, send to proc
void MainWindow:: uart_receiveInfo()
{
    QByteArray info = pCOM->readAll();
    QByteArray hexData = info.toHex();
    LOG_DEBUG<<"\nHEX: "<<hexData;

    uchar msg_tmp[256];
    quint16 length = info.length();
    qDebug()<<"LEN"<<length;
    memcpy(msg_tmp, info, length);

    HEADER *msg = (HEADER*)msg_tmp;
    if(length > 0)
    {
        if(msg->signature == ntohs(SIGNATURE_485)) /*qDebug()<<"sig pass"*/;
        else return ;
        if(msg->address == ADR_BLE_KEY) /*qDebug()<<"addr pass"*/;
        else return ;
        if(checksum_pass(msg) == true) qDebug()<<"checksum pass";
        else return ;

        uart_msg_proc(msg);
    }
}



// uart message route
qint8 MainWindow::uart_msg_proc(HEADER *msg)
{
    if(CMD_BLE_KEY_MAC == msg->cmd) uart_ble_key_mac_rsp(msg);
    else {qDebug()<<"unknown cmd msg";}
    return 0;
}

qint8 MainWindow::uart_ble_key_mac_rsp(HEADER *msg)
{
//    if(1)
//    {
//        timer->stop();
//        flg_getMAC = true;
//    }
    static QString last_MAC;

    LOG_DEBUG<< "HERE";

    if(msg->length == sizeof(BLE_KEY_MAC_MSG_RSP))
    {
        timer->stop();
        BLE_KEY_MAC_MSG_RSP *info = (BLE_KEY_MAC_MSG_RSP *)(msg->data);
        char MAC_str[2*sizeof(BLE_KEY_MAC_MSG_RSP)+1];
        quint8 data_tmp[sizeof(BLE_KEY_MAC_MSG_RSP)];
        memcpy(data_tmp, (quint8*)(&(info->version)),sizeof(BLE_KEY_MAC_MSG_RSP));
        hexString2ascii(MAC_str, data_tmp, sizeof(BLE_KEY_MAC_MSG_RSP));
        MAC_str[sizeof(MAC_str)-1] = '\0';
        qDebug(">>ble MAC is %s, check ", MAC_str);
        QString bleMAC;
        bleMAC += MAC_str;
        LOG_DEBUG<<bleMAC;
        flg_getMAC = true;
        if(last_MAC == bleMAC)
        {
            timer->stop();
        }
        else
        {
            ui->showImeiText_single->setText(bleMAC);
            ui->productInfo_single->setText("");
            this->on_exportLabel_single_clicked();
            last_MAC = bleMAC;
        }
    }

    if(flg_getMAC == true)
    {
        timer->stop();
        flg_getMAC = false;
        timer_next->start(3000);
    }
    return 0;
}

void MainWindow::ble_key_ping(){
    static quint16 cnt=0;

    cnt++;

    if(cnt %1 == 0)//
    {
        unsigned char data = 0;
        ble_key_send_pkg(CMD_BLE_KEY_MAC, &data, 1);
    }
    timer_next->stop();
    timer->start(10);
}

qint8 MainWindow::ble_key_send_pkg(uchar cmd, uchar *data, quint16 len)
{
    HEADER *msg = (HEADER *)malloc(len+6);
    if(msg == NULL) {
     qDebug()<<"err malloc";
     return -1;
    }
    msg->signature=ntohs(SIGNATURE_485);
    msg->address=ADR_BLE_KEY;
    msg->cmd =  cmd;
    msg->length = len;

    memcpy(msg->data, data, len);
    set_check_sum(msg);

//    char *pdata = (char*)msg;
//    qDebug(">> data %x ,%x ,%x ,%x ,%x ,%x ,%x ,%x \n", \
           pdata[0], pdata[1], pdata[2], pdata[3], pdata[4], pdata[5], pdata[6], pdata[7]);

    qDebug("sig: %x , %x, %x, %x",msg->signature,msg->address,msg->cmd,msg->length);
    qDebug()<<"CMD:"<<msg->cmd<<"|send pkg -> size len  "<< sizeof(HEADER)+len+1;
    if(-1 == pCOM->write((char*)msg, 5+len+1))
    {
        qDebug()<<"send err";
        addLog("数据发送失败");
        flg_getMAC = false;
    }
    else
    {
        qDebug("send %x ",msg->data[0]);
//        timer_send->start(5000);//5s
    }

    free(msg);
    return 0;
}
